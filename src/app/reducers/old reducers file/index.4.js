import { combineReducers } from 'redux'
// import todos from './todos'
// import visibilityFilter from './visibilityFilter'

// const todoApp = combineReducers({
//   todos,
//   visibilityFilter
// })

// export default todoApp


const FETCH_ALL_ITEMS = 'FETCH_ALL_ITEMS'
const FETCH_ALL_ITEMS_SUCCESS = 'FETCH_ALL_ITEMS_SUCCESS'
const FETCH_ALL_ITEMS_ERROR = 'FETCH_ALL_ITEMS_ERROR'
const FETCH_ROUTE_ID = 'FETCH_ROUTE_ID'
const FETCH_STOP_ID = 'FETCH_STOP_ID'
const FETCH_TRANSPORT_ID = 'FETCH_TRANSPORT_ID'
const LOAD_ROUTE_THAT_HAVE_STOPS = "LOAD_ROUTE_THAT_HAVE_STOPS"
const LOAD_CROSS_ROUTES  = "LOAD_CROSS_ROUTES "
// export function fetchRouteId(id) {
//   return 
  
  
//   () => {
    
//   }
// }

export function fetchAllItemsFromServer() {
  return (dispatch, getState) => {
    dispatch({type: FETCH_ALL_ITEMS})
    fetch('/static-assets/transport.json')
      .then(res => res.json().then(data => data))
      //.then(res => res.toJSON())
      .then(json => dispatch({type: FETCH_ALL_ITEMS_SUCCESS, payload: json}))
      .catch(errors => dispatch({type: FETCH_ALL_ITEMS_ERROR, errors: errors}))
  }
}

export function fetchRouteById(id) {
  return (dispatch, getState) => {
  //return (dispatch) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      const object = state.json.routes.find(el => el.id == id)
      //stops
      const returnStops = []
      object.stops.map(element => {
        const stop = state.json.stops.find(e => e.id == element)
        returnStops.push(stop)
      }) // [1,3,4,5]
      
      object["returnStops"] = returnStops;
      console.log('object', object);
      const value = object
      dispatch({type: FETCH_ROUTE_ID, payload: value});
    }
  }
}
export function fetchTransportByName(name) {
  return (dispatch, getState) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      //const object = state.json.cars[0].route.id; //передается
      
      const transport = state.json.cars.find(el => el.number == name)
      const routeId = transport.route
      
      
      console.log('fetchTransportByNameroute', state.json.routes.find(route => route.id == routeId))
      const route = state.json.routes.find(route => route.id == routeId)
      
      
      
      const stopsIdsList = route.stops
      const stopsList = stopsIdsList.map(stopId => state.json.stops.find(stop => stop.id == stopId))
      
      transport.stopsList = stopsList
      console.log('fetchTransportByName', transport)
      dispatch({type: FETCH_TRANSPORT_ID, payload: transport});
    }
  }
}

export function fetchStopById(id) {
  return (dispatch, getState) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      const value = state.json.stops.find(el => el.id == id)
      console.log('fetchStopById', value)
      dispatch({type: FETCH_STOP_ID, value: value});
    }
  }
}

// Получение объекта типа res[67] = {id: 67, name: 'название остановки', stops:  [ {id: 4, name: название-маршрута}, {id: 7: name: :название-маршрута}, -//- ] }
export function routeHaveStops() {
  return (dispatch, getState) =>{
    const state = getState()
    console.log('e3222223', state.isDownloaded)
    if ( state.isDownloaded )
    {
      let res={}
      let mass = []

      //const listRoutes = state.json.routes.find(el => el.stops == state.json.stops.id)
      for (let i = 0; i < state.json.stops.length; i++) {
        for (let j = 0; j < state.json.routes.length; j++){
          for (let k = 0; k < state.json.routes[j].stops.length; k++){
           if (state.json.stops[i].id == state.json.routes[j].stops[k]){
             mass.push(state.json.routes[j])
           }
          }
        }
        //res[state.json.stops[i].id] = mass;
        res[state.json.stops[i].id] = {routes: mass, name: state.json.stops[i].name, id: state.json.stops[i].id};
        mass = []
      }
      console.log('RouteHaveStops', res)
      dispatch({type: LOAD_ROUTE_THAT_HAVE_STOPS, payload: res});
    }
  }
}

// Получение маршрута и его пересечений
export function CrossRoutes() {
  return (dispatch, getState) =>{
    const state = getState()
    if ( state.isDownloaded )
    {
      let res={}
      let mass=[]
      let mass_id=[]
      // let data = state.json.routes.map((el, i) => {
      //   state.json.routes.filter(routeJS => {
      //       console.log('rroute', routeJS)
      //       return (el.id!==routeJS.id && 
      //           routeJS.stops.some(stopJS=> {
      //               console.log('sstop', stopJS, el.stops, el.stops.indexOf(stopJS) !== -1)
      //               return (el.stops.indexOf(stopJS) !== -1)
      //           })
      //       )
      //   })})
      //let mass = []
      let crossRoutesObj=[];
      for (let i = 0; i < (state.json.routes.length); i++) {
        for (let j = 0; j < state.json.routes[i].stops.length; j++){
          let route1 = state.json.routes[i].stops[j];
          //console.log("route1 = ", route1);
          for (let l = 0; l < (state.json.routes.length); l++) {
            for (let k = 0; k < state.json.routes[l].stops.length; k++){
              let route2 = state.json.routes[l].stops[k];
              //console.log("route2 = ", route2);
              if (route1 == route2 && i!==l){
                mass.push(state.json.routes[l].name)
                mass_id.push(state.json.routes[l].id)
                //console.log("MASS",mass)
                crossRoutesObj[k] = {id: state.json.routes[l].id, route: state.json.routes[l].name}
              }
            }
          }
        }
        res[state.json.routes[i].id] = { crossRoutesObj, routes: mass, routes_id: mass_id, name: state.json.routes[i].name, id: state.json.routes[i].id};
        mass=[]
        mass_id=[]
        crossRoutesObj=[];
      }
      
      console.log('LOAD_CROSS_ROUTES', res)
      dispatch({type: LOAD_CROSS_ROUTES , payload: res});
    }
  }
}


export function cardReducer(state = {
  json: [],
  isLoading: false,
  isDownloaded: false,
  errors: {},
  route: "K-2", // Хранится список остановок
  routeHaveStops: [], // Связь маршрут - его остановки
  crossRoutes: [],  // Связь Маршрут и его пересечения
  //routes: [],
  mapLoaded: false
}, action) {
  switch (action.type) {
  case FETCH_ALL_ITEMS:
    return Object.assign({}, state, 
    {
      isLoading: true
    }
    )
  case FETCH_ALL_ITEMS_SUCCESS:
    return Object.assign({}, state, 
    {
       isLoading: false,
       isDownloaded: true,
       json: action.payload
    }
    )
  case FETCH_ALL_ITEMS_ERROR:
    return Object.assign({}, state, 
    {
      isLoading: false,
      errors: action.errors
    }
    )
  case FETCH_ROUTE_ID:
      return Object.assign({}, state, 
      {
        route : action.payload,
        //routes: Object.assign({}, state.routes, {[action.payload.id]: action.payload})
      })
  case FETCH_TRANSPORT_ID:
      return Object.assign({}, state, 
      {
        route : action.payload,
      })
  // П
  case LOAD_ROUTE_THAT_HAVE_STOPS:
      return Object.assign({}, state, 
      {
        routeHaveStops : action.payload,
      })
   case LOAD_CROSS_ROUTES:
      return Object.assign({}, state, 
      {
        crossRoutes : action.payload,
      })
      
  default:
    return state
  }
}


// ну и теперь мы можем задиспатчить наш екшен
// store.dispatch(fetchAllItemsFromServer())

// и потом изъять данные из стора
// const state = store.getState()

/*
export const initialState = {
  transports: [],
  routes: [],
  stops: [],
  json: {}
}

export function rootReducer(state = initialState) {
  return state
}

*/