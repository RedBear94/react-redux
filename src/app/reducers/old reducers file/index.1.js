import { combineReducers } from 'redux'
// import todos from './todos'
// import visibilityFilter from './visibilityFilter'

// const todoApp = combineReducers({
//   todos,
//   visibilityFilter
// })

// export default todoApp


const FETCH_ALL_ITEMS = 'FETCH_ALL_ITEMS'
const FETCH_ALL_ITEMS_SUCCESS = 'FETCH_ALL_ITEMS_SUCCESS'
const FETCH_ALL_ITEMS_ERROR = 'FETCH_ALL_ITEMS_ERROR'
const FETCH_ROUTE_ID = 'FETCH_ROUTE_ID'
const FETCH_STOP_ID = 'FETCH_STOP_ID'
const FETCH_TRANSPORT_ID = 'FETCH_TRANSPORT_ID'

// export function fetchRouteId(id) {
//   return 
  
  
//   () => {
    
//   }
// }

export function fetchAllItemsFromServer() {
  return (dispatch, getState) => {
    dispatch({type: FETCH_ALL_ITEMS})
    fetch('/static-assets/transport.json')
      .then(res => res.json().then(data => data))
      //.then(res => res.toJSON())
      .then(json => dispatch({type: FETCH_ALL_ITEMS_SUCCESS, payload: json}))
      .catch(errors => dispatch({type: FETCH_ALL_ITEMS_ERROR, errors: errors}))
  }
}

export function fetchRouteById(id) {
  return (dispatch, getState) => {
  //return (dispatch) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      //const value = {[id] : state.json.routes.find(el => el.id == id)}
      //const e_id = state.json.routes.find(el => el.id == id)
      //const value_id = state.json.routes.find(el => el.id == id)
      const object = state.json.routes.find(el => el.id == id)
      const value = object
      //console.log('object', object.name);
      //const value = {id: , object: object};
      
      //const value = object.name
      dispatch({type: FETCH_ROUTE_ID, payload: value});
    }
  }
}
export function fetchTransportById(id) {
  return (dispatch, getState) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      const value = state.json.transports.find(el => el.id == id)
      console.log('fetchTransportById', value)
      dispatch({type: FETCH_TRANSPORT_ID, payload: value});
    }
  }
}
export function fetchStopById(id) {
  return (dispatch, getState) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      const value = state.json.stops.find(el => el.id == id)
      console.log('fetchStopById', value)
      dispatch({type: FETCH_STOP_ID, value: value});
    }
  }
}

/*export function cardReducer(state = {
  items: [],
  isLoading: false,
  errors: {}
}, action) {
    console.log('state', state);
  switch (action.type) {
  case FETCH_ALL_ITEMS:
    return { ...state,
        isLoading: true
    }
  case FETCH_ALL_ITEMS_SUCCESS:
    return { ...state,
        isLoading: false,
        items: action.payload
    }
  case FETCH_ALL_ITEMS_ERROR:
    return { ...state, 
        isLoading: false,
        errors: action.errors
    }
  default:
    return state
  }
}*/

export function cardReducer(state = {
  json: [],
  isLoading: false,
  isDownloaded: false,
  errors: {},
  route: "K-2",
}, action) {
  switch (action.type) {
  case FETCH_ALL_ITEMS:
    return Object.assign({}, state, 
    {
      isLoading: true
    }
    )
  case FETCH_ALL_ITEMS_SUCCESS:
    return Object.assign({}, state, 
    {
       isLoading: false,
       isDownloaded: true,
       json: action.payload
    }
    )
  case FETCH_ALL_ITEMS_ERROR:
    return Object.assign({}, state, 
    {
      isLoading: false,
      errors: action.errors
    }
    )
  case FETCH_ROUTE_ID:
      return Object.assign({}, state, 
      {
        route: action.payload     //routes {4: {name: "guih", id: 4}}  {[action.payload.id]: action.payload}
        //routes: 'hgfdfghj'
        //routes: Object.assign({}, state.routes, action.value)
        //route: action.payload
      }
      ) 
  default:
    return state
  }
}


// ну и теперь мы можем задиспатчить наш екшен
// store.dispatch(fetchAllItemsFromServer())

// и потом изъять данные из стора
// const state = store.getState()

/*
export const initialState = {
  transports: [],
  routes: [],
  stops: [],
  json: {}
}

export function rootReducer(state = initialState) {
  return state
}

*/