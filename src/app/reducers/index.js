import { combineReducers } from 'redux'
// import todos from './todos'
// import visibilityFilter from './visibilityFilter'

// const todoApp = combineReducers({
//   todos,
//   visibilityFilter
// })

// export default todoApp


const FETCH_ALL_ITEMS = 'FETCH_ALL_ITEMS'
const FETCH_ALL_ITEMS_SUCCESS = 'FETCH_ALL_ITEMS_SUCCESS'
const FETCH_ALL_ITEMS_ERROR = 'FETCH_ALL_ITEMS_ERROR'

const FETCH_ROUTE_ID = 'FETCH_ROUTE_ID'
const FETCH_STOP_ID = 'FETCH_STOP_ID'
const FETCH_TRANSPORT_ID = 'FETCH_TRANSPORT_ID'
const FETCH_USER_EMAIL_PASSWORD = "FETCH_USER_EMAIL_PASSWORD"
const FETCH_USER_NAME_EMAIL_PASSWORDS = "FETCH_USER_NAME_EMAIL_PASSWORDS"
const FETCH_USER_LOGOUT = "FETCH_USER_LOGOUT"

const LOAD_ROUTE_THAT_HAVE_STOPS = "LOAD_ROUTE_THAT_HAVE_STOPS"
const LOAD_CROSS_ROUTES  = "LOAD_CROSS_ROUTES "
const LOAD_MAIN_LIST = "LOAD_MAIN_LIST"
const LOAD_TRANSPORT_ROUTE = "LOAD_TRANSPORT_ROUTE"

const LOAD_FILTRED_STOPS_LIST = "LOAD_FILTRED_STOPS_LIST"
const LOAD_FILTRED_ROUTES_LIST = "LOAD_FILTRED_ROUTES_LIST"
const LOAD_FILTRED_TRANSPORTS_LIST = "LOAD_FILTRED_TRANSPORTS_LIST"
const LOAD_FILTRED_HOMEPAGE_LIST = "LOAD_FILTRED_HOMEPAGE_LIST"

const LOAD_TRANSPORTS_PAGE_LIST = "LOAD_TRANSPORTS_PAGE_LIST"
const LOAD_ROUTES_PAGE_LIST = "LOAD_ROUTES_PAGE_LIST"
const LOAD_STOPS_PAGE_LIST = "LOAD_STOPS_PAGE_LIST"
const LOAD_HOME_PAGE_LIST = "LOAD_HOME_PAGE_LIST"
//const LOAD_MAIN_PAGE_LIST = "LOAD_MAIN_PAGE_LIST"

const SET_DELETE_TRANSPORT = "SET_DELETE_TRANSPORT"
const SET_DELETE_ROUTE = "SET_DELETE_ROUTE"
const SET_DELETE_STOP = "SET_DELETE_STOP"

const SET_ADD_STOP = "SET_ADD_STOP"
const SET_ADD_TRANSPORT = "SET_ADD_TRANSPORT"
const SET_ADD_ROUTE = "SET_ADD_ROUTE"

const FETCH_POINTS_SUCCESS = "FETCH_POINTS_SUCCESS"




export const getOffset  = (countRows, currentNumber) => countRows * currentNumber;
export const getTotalPageCount = (countTotal, countRows) => Math.ceil(countTotal / countRows);



// export function fetchRouteId(id) {
//   return 
  
  
//   () => {
    
//   }
// }

export function fetchAllItemsFromServer() {
  return (dispatch, getState) => {
    dispatch({type: FETCH_ALL_ITEMS})
    fetch('/static-assets/transport.json')
      .then(res => res.json().then(data => data))
      //.then(res => res.toJSON())
      .then(json => dispatch({type: FETCH_ALL_ITEMS_SUCCESS, payload: json}))
      .catch(errors => dispatch({type: FETCH_ALL_ITEMS_ERROR, errors: errors}))
  }
}


export function fetchPointsFromServer(address) {
  return (dispatch, getState) => {
    //console.log('address', address);
    const fullAddress = 'https://geocode-maps.yandex.ru/1.x/?apikey=fecc38f4-b1f4-4eba-bccf-b6f403349f86&format=json&results=1&geocode='+address;
    //console.log('address', fullAddress);
    fetch(fullAddress)
      .then(res => res.json().then(data => data))
      //.then(res => res.toJSON())
      .then(json => 
      {
        //console.log('APIjson', json);
        const points = json.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos.split(' ');
        const p = {y: points[0], x: points[1]}
        //console.log('points', points);
        dispatch({type: FETCH_POINTS_SUCCESS, payload: p}) }
        )
      .catch(errors => {
        //console.log('errors', errors);
        dispatch({type: FETCH_ALL_ITEMS_ERROR, errors: errors})
        
        }
        )
  }
}


export function fetchRouteById(id) {
  return (dispatch, getState) => {
  //return (dispatch) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      const object = state.json.routes.find(el => el.id == id)
      //stops
      const returnStops = []
      object.stops.map(element => {
        const stop = state.json.stops.find(e => e.id == element)
        returnStops.push(stop)
      }) // [1,3,4,5]
      
      object["returnStops"] = returnStops;
      //console.log('object', object);
      const value = object
      dispatch({type: FETCH_ROUTE_ID, payload: value});
    }
  }
}
export function fetchTransportByName(name) {
  return (dispatch, getState) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      //const object = state.json.cars[0].route.id; //передается
      
      const transport = state.json.cars.find(el => el.number == name)
      const routeId = transport.route
      
      
      //console.log('fetchTransportByNameroute', state.json.routes.find(route => route.id == routeId))
      const route = state.json.routes.find(route => route.id == routeId)
      
      
      
      const stopsIdsList = route.stops
      const stopsList = stopsIdsList.map(stopId => state.json.stops.find(stop => stop.id == stopId))
      
      transport.stopsList = stopsList
      //console.log('fetchTransportByName', transport)
      dispatch({type: FETCH_TRANSPORT_ID, payload: transport});
    }
  }
}

export function fetchStopById(id) {
  return (dispatch, getState) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      const stop = state.json.stops.find(el => el.id == id)
      stop.routes = state.json.routes.filter(route => route.stops.indexOf(stop.id)!==-1)
      //console.log('fetchStopById', stop)
      dispatch({type: FETCH_STOP_ID, payload: stop});
    }
  }
}

export function fetchUserByEmailAndPassword(email, password) {
  return (dispatch, getState) => {
    const state = getState()
    if ( state.isDownloaded )
    {
      const user = state.json.users.find(user => user.email == email && user.password == password)
      //console.log('fetchUserByEmailAndPassword', user)
      dispatch({type: FETCH_USER_EMAIL_PASSWORD, payload: user});
    }
  }
}

export function fetchUserByNameEmailPasswords(name, email, password1, password2) {
  return (dispatch, getState) => {
    const state = getState()
    if ( state.isDownloaded && name && email && password1 && password1 === password2)
    {
      const id = 1 + Math.max.apply(Math, state.json.users.map(user => user.id))
      const user = { id: id, name: name, email: email, password: password1}
      //console.log('fetchUserByNameEmailPasswords', user)
      dispatch({type: FETCH_USER_NAME_EMAIL_PASSWORDS, payload: user});
    }
  }
}

export function fetchUserLogout() {
  return (dispatch, getState) => {
      dispatch({type: FETCH_USER_LOGOUT});
  }
}

// export function getMainPage(pageNumber) {
//   return (dispatch, getState) =>{
//     const state = getState();
//     const totalPages = getTotalPageCount(state.homePageData.length, state.countRows);
//     pageNumber = pageNumber >= totalPages ? pageNumber = totalPages-1: pageNumber;
//     const offset = getOffset(state.countRows, pageNumber);
//     const pageRows = state.homePageData.slice(offset, offset+state.countRows);
//     const payload = {pageRows: pageRows, activePage: pageNumber, totalPages: totalPages}
//     dispatch({type: LOAD_MAIN_PAGE_LIST, payload: payload});
//   }
// }

export function getStopPage(pageNumber) {
  return (dispatch, getState) =>{
    const state = getState();
    const totalPages = getTotalPageCount(state.stops.length, state.countRows);
    pageNumber = pageNumber >= totalPages ? pageNumber = totalPages-1: pageNumber;
    const offset = getOffset(state.countRows, pageNumber);
    const pageRows = state.stops.slice(offset, offset+state.countRows);
    const payload = {pageRows: pageRows, activePage: pageNumber, totalPages: totalPages}
    dispatch({type: LOAD_STOPS_PAGE_LIST, payload: payload});
  }
}

export function getTransportPage(pageNumber) {
  return (dispatch, getState) =>{
    const state = getState();
    const totalPages = getTotalPageCount(state.transports.length, state.countRows);
    pageNumber = pageNumber >= totalPages ? pageNumber = totalPages-1: pageNumber
    const offset = getOffset(state.countRows, pageNumber);
    const pageRows = state.transports.slice(offset, offset+state.countRows);
    const payload = {pageRows: pageRows, activePage: pageNumber, totalPages: totalPages}
    dispatch({type: LOAD_TRANSPORTS_PAGE_LIST, payload: payload});
  }
}

export function getRoutePage(pageNumber) {
  return (dispatch, getState) =>{
    const state = getState();
    const totalPages = getTotalPageCount(state.routes.length, state.countRows);
    pageNumber = pageNumber >= totalPages ? pageNumber = totalPages-1: pageNumber;
    const offset = getOffset(state.countRows, pageNumber);
    const pageRows = state.routes.slice(offset, offset+state.countRows);
    const payload = {pageRows: pageRows, activePage: pageNumber, totalPages: totalPages}
    //console.log("LOAD_ROUTES_PAGE_LIST", payload)
    dispatch({type: LOAD_ROUTES_PAGE_LIST, payload: payload});
  }
}

export function getHomePage(pageNumber) {
  return (dispatch, getState) =>{
    const state = getState();
    const totalPages = getTotalPageCount(state.searchdata.length, state.countRows);
    pageNumber = pageNumber >= totalPages ? pageNumber = totalPages-1: pageNumber;
    const offset = getOffset(state.countRows, pageNumber);
    const pageRows = state.searchdata.slice(offset, offset+state.countRows);
    const payload = {pageRows: pageRows, activePage: pageNumber, totalPages: totalPages}
    dispatch({type: LOAD_HOME_PAGE_LIST, payload: payload});
  }
}

export function getStopsBlank(){
   return (dispatch, getState) => {
       //console.log("routeHaveStops!",  getState().routeHaveStops)
       dispatch({type: LOAD_FILTRED_STOPS_LIST, payload: getState().routeHaveStops});
   }
}

export function getStopsFilter(stopName="", stopRoute="", sort=""){
  return (dispatch, getState) =>{
      let stops = getState().routeHaveStops;
      
      if (stopName) {
        const stopNameUp = stopName.toUpperCase()
        stops = stops.filter(stop =>stop.name.toUpperCase().indexOf(stopNameUp)!==-1)
      }
      
      if (stopRoute) {
        const stopRouteUp = stopRoute.toUpperCase()
        stops = stops.filter(stop =>stop.routes.some(r => r.name.toUpperCase().indexOf(stopRouteUp)!==-1))
      }
      
      //console.log("FilterStops", stops)
      
      dispatch({type: LOAD_FILTRED_STOPS_LIST, payload: stops});
  }
}

export function getRoutesBlank(){
  return (dispatch, getState) =>{
       dispatch({type: LOAD_FILTRED_ROUTES_LIST, payload: getState().crossRoutes});
  }
}

export function getRoutesFilter(routeName="", routeCross="", sort=""){
  return (dispatch, getState) =>{
      let routes = getState().crossRoutes;
      //console.log('getRoutesFilter1', routes)
      if (routeName) {
        const routeNameUp = routeName.toUpperCase()
        routes = routes.filter(route =>route.name.toUpperCase().indexOf(routeNameUp)!==-1)
      }
      
      //console.log('getRoutesFilter2', routes)
      if (routeCross) {
        const routeCrossUp = routeCross.toUpperCase()
        routes = routes.filter(route =>route.crossRoutesObj.some(r => r.name.toUpperCase().indexOf(routeCrossUp)!==-1))
      }
      
      //console.log('getRoutesFilter3', routes)
      //console.log("FilterRoutes", routes)
      
      dispatch({type: LOAD_FILTRED_ROUTES_LIST, payload: routes});
  }
}


export function getTransportsBlank(){
  return (dispatch, getState) =>{
       dispatch({type: LOAD_FILTRED_TRANSPORTS_LIST, payload: getState().transportsRoute});
  }
}



export function getTransportsFilter(transportNumber="", transportType="", transportRoute="", transportSeats="", sort=""){
  return (dispatch, getState) =>{
      let transports = getState().transportsRoute;
      //console.log('gettransportsFilter1', transports)
      if (transportNumber) {
        const transportNumberUp = transportNumber.toUpperCase()
        transports = transports.filter(transport =>transport.number.toUpperCase().indexOf(transportNumberUp)!==-1)
      }
      if (transportType) {
        const transportTypeUp = transportType.toUpperCase()
        transports = transports.filter(transport =>transport.type.toUpperCase().indexOf(transportTypeUp)!==-1)
      }
      if (transportRoute) {
        const transportRouteUp = transportRoute.toUpperCase()
        transports = transports.filter(transport =>transport.routeObj.name.toUpperCase().indexOf(transportRouteUp)!==-1)
      }
      if (transportSeats) {
        const transportSeatsUp = transportSeats.toUpperCase()
        transports = transports.filter(transport =>transport.places.toUpperCase().indexOf(transportSeatsUp)!==-1)
      }
      
      // console.log('getRoutesFilter2', routes)
      // if (routeCross) {
      //   const routeCrossUp = routeCross.toUpperCase()
      //   routes = routes.filter(route =>route.crossRoutesObj.some(r => r.name.toUpperCase().indexOf(routeCrossUp)!==-1))
      // }
      
      //console.log('gettransportsFilter3', transports)
      //console.log("Filtertransports", transports)
      
      dispatch({type: LOAD_FILTRED_TRANSPORTS_LIST, payload: transports});
  }
}

export function deleteRowTransport(transport) {
  return (dispatch, getState) =>{
      //console.log('deleteRowTransport', transport)
      // TODO delete transport from json, transportsRoute and transports and other tables.
      let {json, transportsRoute, transports, transportsPage} = getState();
      json.cars = json.cars.filter(car => transport.number !== car.number)
      transportsRoute = transportsRoute.filter(car => transport.number !== car.number)
      transports = transports.filter(car => transport.number !== car.number)
      transportsPage = transportsPage.filter(car => transport.number !== car.number)
      
      //console.log('SET_DELETE_TRANSPORT', json, transports,transportsPage)
      
      
      dispatch({type: SET_DELETE_TRANSPORT, payload: {json, transportsRoute, transports, transportsPage}});
  }
}

export function deleteRowRoute(route) { // TODO
  return (dispatch, getState) =>{
      //console.log('deleteRowRoute', route)
      const json= getState().json;
  
      json.routes = json.routes.filter(r => route.id !== r.id)
      json.cars = json.cars.map(transport => 
        transport.route == route.id ? { number: transport.number, type: transport.type, places: transport.places, route: null} : transport
      )
           
      //console.log('SET_DELETE_ROUTE', json)
      
      dispatch({type: SET_DELETE_ROUTE, payload: json});
      
      
      // routeHaveStops()
      // getStopsBlank()
      // CrossRoutes()
      // getRoutesBlank() 
      // getTransportsRoute()
      // getTransportsBlank()
      // getHomeData()
      // getHomePageBlank()
      
      // transportsRoute = transportsRoute.filter(car => transport.number !== car.number)
      // transports = transports.filter(car => transport.number !== car.number)
      // transportsPage = transportsPage.filter(car => transport.number !== car.number)
 
  }
}

export function deleteRowStop(stop) { // TODO
  return (dispatch, getState) =>{
      //console.log('deleteRowStop', stop)
      const json = getState().json;
      
      const newStops = json.stops.filter(s => stop.id !== s.id)
      //console.log('deleteRowStopnewStops', newStops)
      json.routes = json.routes.map(route => {
        return route.stops.indexOf(stop.id) !=-1 ? { id: route.id, name: route.name, time_start: route.time_start, time_stop: route.time_stop,
          stops: route.stops.filter(st => stop.id !== st)
          } : route
      }
      )
      json.stops = newStops
      
      //console.log('SET_DELETE_STOP', json)
      
      
      dispatch({type: SET_DELETE_STOP, payload: json});
  }
}


export function addRowStop(name, x, y, id=null) {
  return (dispatch, getState) =>{
    const json = getState().json;
    //console.log('addRowStop', name, x, y, id)
    if (id && json.stops.find(stop=> stop.id == id))
      json.stops = json.stops.map(stop => 
        stop.id == id ? {id, name, point: {x, y}} : stop
      )
    else
      json.stops.push({id: 1 + Math.max.apply(Math, json.stops.map(stop => stop.id)), name, point: {x, y}})
    dispatch({type: SET_ADD_TRANSPORT, payload: json});
    
    
      // id = id || 
      // const stop = {id, name, point: {x, y}}
      // console.log('addRowStop', stop)
      // dispatch({type: SET_ADD_STOP, payload: stop});
      
      // id = id || 1 + Math.max.apply(Math, getState().json.stops.map(stop => stop.id))
      // const stop = {id, name, point: {x, y}}
      // console.log('addRowStop', stop)
      // dispatch({type: SET_ADD_STOP, payload: stop});
  }
}
export function addRowTransport(number, type, places, route) {
  return (dispatch, getState) =>{
    const json = getState().json;
    if (json.cars.find(transport=> transport.number == number))
      json.cars = json.cars.map(transport => 
        transport.number == number ? {number, type, places, route} : transport
      )
    else
      json.cars.push({number, type, places, route})
    dispatch({type: SET_ADD_STOP, payload: json});
  }
}
export function addRowRoute(name, stops, time_start, time_stop, id = null) {
  return (dispatch, getState) =>{
    const json = getState().json;
    const newStopsId = stops.map(stop => Number(stop))
    //console.log('addRowRoute', name, newStopsId, time_start, time_stop, id)
    if (id && json.routes.find(route => route.id == id))
      json.routes = json.routes.map(route => 
        route.id == id ? {id, name, stops: newStopsId, time_start, time_stop} : route
      )
    else
      json.routes.push({id: 1 + Math.max.apply(Math, json.routes.map(route => route.id)), name, stops: newStopsId, time_start, time_stop})
    dispatch({type: SET_ADD_ROUTE, payload: json});
    
    
      // id = id || 1 + Math.max.apply(Math, getState().json.routes.map(route => route.id))
      // const route = {id, name, stops, time_start, time_stop}
      // console.log('addRowRoute', route)
      // dispatch({type: SET_ADD_ROUTE, payload: route});
  }
}


export function getHomePageBlank(){
  return (dispatch, getState) =>{
       dispatch({type: LOAD_FILTRED_HOMEPAGE_LIST, payload: getState().homePageData});
  }
}

export function getHomePageFilter(homeStop1="", homeStop2="", homeTime=""){
  return (dispatch, getState) =>{
      let routes = getState().homePageData;
      //console.log('getHomePageFilter1', routes)
      if (homeStop1) {
        const homeStop1Up = homeStop1.toUpperCase()
        routes = routes.filter(route =>route.stopsObj.some(stop => stop.name.toUpperCase().indexOf(homeStop1Up)!==-1))
      }
      if (homeStop2) {
        const homeStop2Up = homeStop2.toUpperCase()
        routes = routes.filter(route =>route.stopsObj.some(stop => stop.name.toUpperCase().indexOf(homeStop2Up)!==-1))
      }
      
      if (homeTime) {
        
        routes = routes.filter(route => route.time_start <= homeTime && route.time_stop >= homeTime)
      }

      
      
      //console.log('getHomePageFilter3', routes)
      
      dispatch({type: LOAD_FILTRED_HOMEPAGE_LIST, payload: routes});
  }
}

// Получение объекта типа res[67] = {id: 67, name: 'название остановки', stops:  [ {id: 4, name: название-маршрута}, {id: 7: name: :название-маршрута}, -//- ] }
export function routeHaveStops() {
  return (dispatch, getState) =>{
    //console.log('Dispathing...')
    const state = getState()
    //console.log('e3222223', state.isDownloaded)
    if ( state.isDownloaded )
    {
      //let res={}
      const res=[]
      

      //const listRoutes = state.json.routes.find(el => el.stops == state.json.stops.id)
      for (let i = 0; i < state.json.stops.length; i++) {
        const mass = []
        for (let j = 0; j < state.json.routes.length; j++){
          for (let k = 0; k < state.json.routes[j].stops.length; k++){
           if (state.json.stops[i].id == state.json.routes[j].stops[k]){
             mass.push(state.json.routes[j])
           }
          }
        }
        //res[state.json.stops[i].id] = mass;
        //res[state.json.stops[i].id] = {routes: mass, name: state.json.stops[i].name, id: state.json.stops[i].id};
         res.push({routes: mass, name: state.json.stops[i].name, id: state.json.stops[i].id});
        //mass = []
      }
      //console.log('RouteHaveStops', res)
      dispatch({type: LOAD_ROUTE_THAT_HAVE_STOPS, payload: res});
    }
  }
}

// Получение маршрута и его пересечений
export function CrossRoutes() {
  return (dispatch, getState) =>{
    //console.log('Dispathing...')
    const state = getState()
    if ( state.isDownloaded )
    {
      //let res={}
      const res=[]
      let crossRoutesObj=[];
      
      for (let i = 0; i < (state.json.routes.length); i++) {
        for (let j = 0; j < state.json.routes[i].stops.length; j++){
          let route1 = state.json.routes[i].stops[j];
          for (let l = 0; l < (state.json.routes.length); l++) {
            for (let k = 0; k < state.json.routes[l].stops.length; k++){
              let route2 = state.json.routes[l].stops[k];
              if (route1 == route2 && i!==l){
                crossRoutesObj.push({id: state.json.routes[l].id, name: state.json.routes[l].name})
              }
            }
          }
        }
        //res[state.json.routes[i].id] = { crossRoutesObj, name: state.json.routes[i].name, id: state.json.routes[i].id};
        res.push({ crossRoutesObj, name: state.json.routes[i].name, id: state.json.routes[i].id});
        crossRoutesObj=[];
      }
      
      //console.log('LOAD_CROSS_ROUTES', res)
      dispatch({type: LOAD_CROSS_ROUTES , payload: res});
    }
  }
}

export function getTransportsRoute() {
  return (dispatch, getState) =>{
    //console.log('Dispathing...')
      const state = getState();
      const transports = state.json.cars;
      //console.log('transports',transports)
      // homeData = [routes {id: 213, name:"route_name1", stops: [{id: 1, name: "stop_name1"},{},...,{}]}]
      const transportsData = []
      
      transports.forEach(transport => {
        const nTransport = transport;
        
        nTransport.routeObj = state.json.routes.find(route => route.id == transport.route)
        
        transportsData.push(nTransport);
      })
      
      //console.log('homeData', homeData)
      dispatch({type: LOAD_TRANSPORT_ROUTE, payload: transportsData});
   }
}
export function getHomeData(){
   return (dispatch, getState) =>{
      const state = getState();
      const routes = state.json.routes;
      // homeData = [routes {id: 213, name:"route_name1", stops: [{id: 1, name: "stop_name1"},{},...,{}]}]
      const homeData = []
      
      routes.forEach(route => {
        const nRoute = route;
        
        nRoute.stopsObj = route.stops.map(stopId => state.json.stops.find(stop => stop.id === stopId))
        
        homeData.push(nRoute);
      })
      
      //console.log('homeData', homeData)
      dispatch({type: LOAD_MAIN_LIST, payload: homeData});
   }
}


export function cardReducer(state = {
  json: [],
  isLoading: false,
  isDownloaded: false,
  errors: {},
  route: "K-2", // Хранится список остановок
  routeHaveStops: [], // Связь маршрут - его остановки // stops
  crossRoutes: [],  // Связь Маршрут и его пересечения //routes
  homePageData: [],
  searchdata: [],
  transportsRoute: [],
  stop: undefined,
  user: undefined,
  mapLoaded: false,
  stops: [],
  transports: [],
  routes: [],
  totalPages: 0,
  countRows: 5,
  transportsPage: [],
  routesPage: [],
  stopsPage: [],
  homePage: [],
  mainPage: [],
}, action) {
  switch (action.type) {
  case FETCH_ALL_ITEMS:
    return Object.assign({}, state, 
    {
      isLoading: true
    }
    )
  case FETCH_ALL_ITEMS_SUCCESS:
    return Object.assign({}, state, 
    {
       isLoading: false,
       isDownloaded: true,
       json: action.payload
    }
    )
  case FETCH_POINTS_SUCCESS:
    return Object.assign({}, state, 
    {
       points: action.payload
    }
    )
  case FETCH_ALL_ITEMS_ERROR:
    return Object.assign({}, state, 
    {
      isLoading: false,
      errors: action.errors
    }
    )
  case FETCH_ROUTE_ID:
      return Object.assign({}, state, 
      {
        route : action.payload,
        //routes: Object.assign({}, state.routes, {[action.payload.id]: action.payload})
      }) 
  case FETCH_STOP_ID:
      return Object.assign({}, state, 
      {
        stop : action.payload,
      })
  case FETCH_TRANSPORT_ID:
      return Object.assign({}, state, 
      {
        route : action.payload,
      })
  // П
  case LOAD_ROUTE_THAT_HAVE_STOPS:
      return Object.assign({}, state, 
      {
        routeHaveStops : action.payload,
      })
  case LOAD_CROSS_ROUTES:
      return Object.assign({}, state, 
      {
        crossRoutes : action.payload,
      })
  case LOAD_TRANSPORT_ROUTE:
      return Object.assign({}, state, 
      {
        transportsRoute : action.payload,
      })    
  case LOAD_MAIN_LIST:
      return Object.assign({}, state, 
      {
        homePageData: action.payload,
      })
      
      
  case FETCH_USER_EMAIL_PASSWORD:
      return Object.assign({}, state, 
      {
        user : action.payload,
      })
      
      
  case FETCH_USER_NAME_EMAIL_PASSWORDS:
    return Object.assign({}, state, 
    {
      json: Object.assign({}, state.json,  { users: [...state.json.users, action.payload] }),
      user: action.payload,
    })
       
  case FETCH_USER_LOGOUT:
    return Object.assign({}, state, 
    {
      user: undefined,
    })
      
      
  case LOAD_FILTRED_STOPS_LIST:
    return Object.assign({}, state, 
    {
      stops: action.payload,
    })  
  case LOAD_FILTRED_ROUTES_LIST:
    return Object.assign({}, state, 
    {
      routes: action.payload,
    })  
  case LOAD_FILTRED_TRANSPORTS_LIST:
    return Object.assign({}, state, 
    {
      transports: action.payload,
    })
  case LOAD_FILTRED_HOMEPAGE_LIST:
    return Object.assign({}, state, 
    {
      searchdata: action.payload,
    })
            
// case LOAD_MAIN_PAGE_LIST:
//     return Object.assign({}, state, 
//     {
//       mainPage: action.payload.pageRows,
//       activePage:  action.payload.activePage, 
//       totalPages:  action.payload.totalPages,
//     }) 
  case LOAD_STOPS_PAGE_LIST:
    return Object.assign({}, state, 
    {
       stopsPage: action.payload.pageRows,
       activePage:  action.payload.activePage, 
       totalPages:  action.payload.totalPages,
    })  
  case LOAD_ROUTES_PAGE_LIST:
    return Object.assign({}, state, 
    {
       routesPage: action.payload.pageRows,
       activePage:  action.payload.activePage, 
       totalPages:  action.payload.totalPages,
    })  
  case LOAD_TRANSPORTS_PAGE_LIST:
    return Object.assign({}, state,  
    {
       transportsPage: action.payload.pageRows,
       activePage:  action.payload.activePage, 
       totalPages:  action.payload.totalPages,
    })
  case LOAD_HOME_PAGE_LIST:
    return Object.assign({}, state, 
    {
       homesPage: action.payload.pageRows,
       activePage:  action.payload.activePage, 
       totalPages:  action.payload.totalPages,
    })
    
  case SET_DELETE_TRANSPORT: 
      return Object.assign({}, state, 
    {
       json: Object.assign({}, state.json,  { cars: action.payload.json.cars }),
       transportsRoute: action.payload.transportsRoute,
       transports: action.payload.transports, 
       transportsPage: action.payload.transportsPage,
    })
    
  case SET_DELETE_ROUTE: 
      return Object.assign({}, state, 
    { // TODO
      json: action.payload,
      // json: Object.assign({}, state.json,  { cars: action.payload.json.cars }),
      // transportsRoute: action.payload.transportsRoute,
      // transports: action.payload.transports, 
      // transportsPage: action.payload.transportsPage,
    })
    
  case SET_DELETE_STOP: 
      return Object.assign({}, state, 
    { // TODO
    
      json: action.payload,
      // json: Object.assign({}, state.json,  { cars: action.payload.json.cars }),
      // transportsRoute: action.payload.transportsRoute,
      // transports: action.payload.transports, 
      // transportsPage: action.payload.transportsPage,
    })
    
        
  case SET_ADD_STOP:
    return Object.assign({}, state, 
    {
      // json: Object.assign({}, state.json,  { stops: [...state.json.stops, action.payload] }),
      json: action.payload,
    })
    
  case SET_ADD_TRANSPORT:
    return Object.assign({}, state, 
    {
      // json: Object.assign({}, state.json,  { cars: [...state.json.cars, action.payload] }),
      json: action.payload,
    })    
  case SET_ADD_ROUTE:
    return Object.assign({}, state, 
    {
      // json: Object.assign({}, state.json,  { routes: [...state.json.routes, action.payload] }),
      json: action.payload,
    })
    
    
      
  default:
    return state
  }
}


// ну и теперь мы можем задиспатчить наш екшен
// store.dispatch(fetchAllItemsFromServer())

// и потом изъять данные из стора
// const state = store.getState()

/*
export const initialState = {
  transports: [],
  routes: [],
  stops: [],
  json: {}
}

export function rootReducer(state = initialState) {
  return state
}

*/