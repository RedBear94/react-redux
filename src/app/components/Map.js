import React, { Component } from 'react'

function init (i, points) {
    //console.log('Mapints', i)
    return function () {
        var multiRoute = new ymaps.multiRouter.MultiRoute({
            // Описание опорных точек мультимаршрута.
            referencePoints: points ? points : [[]], //points,
            params: {
                results: 1
            }
        }, {
            boundsAutoApply: true
        });
    
    
        // Создаем карту с добавленными на нее кнопками.
        var myMap = new ymaps.Map('map', {
            center: [59.919289, 30.335124],
            zoom: 7,
            //controls: [trafficButton, viaPointButton]
        }, {
            buttonMaxWidth: 300
        });
    
        // Добавляем мультимаршрут на карту.
        myMap.geoObjects.add(multiRoute);
    }
}



class Map extends Component {
    constructor(props) {
        super(props);
        this.props=props;
        this.state = {value: 0, notFirst: false, flagEndSend: false, points: null};
        this.changedProps = this.changedProps.bind(this);
        this.onIncrease = this.onIncrease.bind(this);
    } 
    
    changedProps(){
      this.setState(prevState => ({
        points: prevState.points = this.props.points
      }))
    }
    
    onIncrease(){
      this.setState(prevState => ({
        value: prevState.value + 1
      }))
    }
    
    componentWillUnmount() {
        this.state.notFirst = true
        this.state.flagEndSend = false
        //console.log("WillUnmount", this.state.notFirst)
    }
    
    render() {
        //console.log('mapprops', this.props)
        //console.log('mapstate', this.state)
        
        // Проверка на существование
        if(this.state.value == 0){
            if(this.props.points){
                if(this.state.notFirst == false){
                        //console.log("first if");
                        this.setState({notFirst: this.state.notFirst = true});
                        {this.onIncrease()}
                        //console.log("value = ", this.state.value)
                }
            }
        }
        // Проверка на доставку
        else{
            if(this.state.value == 1){
                if(this.props.points){
                    if (this.state.notFirst == true){
                        if(this.state.flagEndSend == false){
                            //console.log("second if");
                            this.setState({flagEndSend: this.state.flagEndSend = true});
                            {this.onIncrease()}
                            //console.log("value = ", this.state.value)
                        }
                    }
                }
            }
        }
        // Передача данных в this.state.point
        if(this.state.value == 2){
            if(this.state.points == null){
                if(this.state.flagEndSend == true){
                    //console.log("third if");
                    this.changedProps();
                    {this.onIncrease()}
                    //console.log("value = ", this.state.value)
                }
            }
        }
        // Вывод на карту
        if(this.state.value == 3){
            ymaps.ready(init(0, this.state.points));
            {this.onIncrease()}
            //console.log("value = ", this.state.value)
        }
        
        
        return (
          <div className="map" id="map"></div>
        );
    }
}


export default Map