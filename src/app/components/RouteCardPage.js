import React, { Component } from 'react'
import Card from './Card'

import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';

import { fetchRouteById } from '../reducers'

class RouteCardPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        this.state = {id: props.match.params.id}

        this.props.getRouteById(this.state.id)
    }
    render() {

        return (
            <div>
                { this.props.isDownloaded  ? 
                <div>
                    <Card 
                        title="Route"
                        name={this.props.route.name}
                        points= {this.props.route.returnStops}
                    />
                </div>
                    : ''
                }
            </div>
        );
    }
}

const mapStateToProps = store => {
  //console.log('mapStateToProps', store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        getRouteById: id => dispatch(fetchRouteById(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RouteCardPage)