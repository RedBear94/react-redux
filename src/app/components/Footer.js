import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';

class Footer extends Component {

    render() {
        return (
            <div>
               <section className="footer-light">
                	<div className="container main_text bottom-nav">
                			<div className="div-size-1 bottom-nav-border">
                			<h3>Contacts</h3>
                			<p>2018 MyCorp</p>
                			<p>Our Telefon +7 (000) 000-00-00</p>
                			</div>
                			
                			<div className="div-size-1 bottom-nav-border">
                				<h3>Navigation</h3>
                				<nav className="nav flex-column">
                					<NavLink className="nav-link" activeClassName="selected" exact to="/">Home</NavLink>
                					<NavLink className="nav-link" activeClassName="selected" exact to="/stops">Stops</NavLink>
                					<NavLink className="nav-link" activeClassName="selected" exact to="/routes">Routes</NavLink>
                					<NavLink className="nav-link" activeClassName="selected" exact to="/transports">Car park</NavLink>
                				</nav>
                			</div>
                		
                			<div className="div-size-1 bottom-info">
                				<h3>Authorization</h3>
                				<nav className="nav flex-column">
                					{ this.props.isUser ? <NavLink className="nav-link" activeClassName="selected" exact to="/account">Account</NavLink> : '' }
                					{ this.props.isUser ? <NavLink className="nav-link" activeClassName="selected" exact to="/logout">Log out</NavLink> : '' }
                					{ !this.props.isUser ? <NavLink className="nav-link" activeClassName="selected" exact to="/authorization">Log in</NavLink> : '' }
                					{ !this.props.isUser ? <NavLink className="nav-link" activeClassName="selected" exact to="/registration">Registration</NavLink> : '' }
                				</nav>
                			</div>
                	</div>
                </section>
                <footer className="footer-dark">	
                	<div className="container">
                		<div className="copyright">
                			<p><span className="hide-copyright">2018 Transportation information system</span></p>
                		</div>
                	</div>
                </footer>
            </div>
        );
    }
}


export default Footer