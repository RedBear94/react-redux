import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';

class RoutesList extends Component{

    render() {
        return (
        <div>
            <h3>Routes list:</h3>
            <ul className="routes-list">
                {
                    this.props.routes ?
                    this.props.routes.map(route => {
                        return <li><NavLink to={'/routes/'+route.id}>{route.name}</NavLink></li>
                    })
                    : <p>No routes</p>
                }
                </ul>
         </div>
        );
    }
}

export default RoutesList