import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { getRoutesFilter, getRoutePage, addRowRoute, deleteRowRoute} from '../reducers'
import { CrossRoutes, getRoutesBlank,  routeHaveStops, getStopsBlank, getTransportsRoute, getTransportsBlank, getHomeData, getHomePageBlank} from '../reducers'

class RouteListPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        //console.log('TTT', this.props.crossRoutes)
        //this.props.getCrossRoutes()
        !this.props.crossRoutes.length ? this.props.getCrossRoutes() : ''
        !this.props.routes.length ? this.props.getRoutesBlank() : ''
        //this.props.getRoutesBlank()
        
        this.updateDataRoute = this.updateDataRoute.bind(this);
        this.state = {
            routeName: '',
            routeCross: '',
            sort: '',
            page: 0,
            isAdding: false,
            isFieldsHave: true,
            newStops: [],
            }
        this.props.getRoutePage(this.state.page) 
        
        this.changeisAdding = this.changeisAdding.bind(this);
        this.handleChangeNewName = this.handleChangeNewName.bind(this)
        this.handleChangeNewStops = this.handleChangeNewStops.bind(this)
        this.handleChangeNewTimeStart = this.handleChangeNewTimeStart.bind(this)
        this.handleChangeNewTimeStop = this.handleChangeNewTimeStop.bind(this)
        this.handleAddStop = this.handleAddStop.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        
        this.deleteStop = this.deleteStop.bind(this)
        this.deleteRow = this.deleteRow.bind(this)
        this.editRow = this.editRow.bind(this)
    }
    
      
     updateDataRoute  (value) { 
            //console.log("RoutesList", value)
            const t = this.state; // old
            t[value.id] = value.value;
            this.setState({[value.id]: value.value}) 
            this.props.getRoutesFilter(t.routeName, t.routeCross, t.sort)
            this.props.getRoutePage(0)
        }
        
     deleteRow (route) { 
         return () => {
            //console.log("RouteDelete", route);
            this.props.deleteRowRoute( route )
            // this.props.getRoutePage(this.props.activePage)
            
            this.props.routeHaveStops();
            this.props.getStopsBlank();
            this.props.getCrossRoutes();
            this.props.getRoutesBlank(); 
            this.props.getTransportsRoute();
            this.props.getTransportsBlank();
            this.props.getHomeData();
            this.props.getHomePageBlank();
            this.props.getRoutePage(this.props.activePage)
         }
        }
        

     editRow (route) { 
         return () => {
            //console.log("TransportEdit", route);
            const routeObj = this.props.json.routes.find(r => r.id == route.id)
            this.setState({
                isAdding: true,
                isEditting: true,
                newId: routeObj.id,
                newName: routeObj.name,
                newStops: routeObj.stops,
                newTimeStart: routeObj.time_start, 
                newTimeStop: routeObj.time_stop,
            })
         }
        }
        
     pageChange (pageNumber) { 
         return () => {
            //console.log("pageNumber", pageNumber)
            this.setState({page: pageNumber})
            this.props.getRoutePage(pageNumber)
        }
    }
    
       changeisAdding(){
        this.setState(prevState => ({
            newId: null, 
            newName: null,
            newStops:  [], 
            newTimeStart: null,
            newTimeStop: null,
            isEditting: false,
            isAdding: !prevState.isAdding
        }))
    }  
    handleChangeNewName(event) {
        this.setState({newName: event.target.value});
    }
    
    handleChangeNewStops(event) {
        //console.log('handleChangeNewStops', event.target.name);
        const newStops = this.state.newStops;
        newStops[event.target.name] = event.target.value
        this.setState({newStops});
        //this.newStops()
    }
    
    deleteStop(stopI) {
        return () => {
        //console.log('deleteStop', stopI);
        const newStops =  this.state.newStops;
        newStops.splice(stopI, 1);
        this.setState({newStops});
    }
    }
    
    
    handleChangeNewTimeStart(event) {
        this.setState({newTimeStart: event.target.value});
    }
    handleChangeNewTimeStop(event) {
        this.setState({newTimeStop: event.target.value});
    }
    
    handleSubmit (event) {
        event.preventDefault();
        //console.log('handleSubmit', this.state.newName, this.state.newStops , this.state.newTimeStart , this.state.newTimeStop, this.state.newId)
        if ( this.state.newName && this.state.newStops && this.state.newTimeStart && this.state.newTimeStop ){
            //console.log('form is submitted');

                this.setState({isAdding: true, isSuccess: true, isWrong: false,  isFieldsHave: true});
                this.props.addRowRoute(this.state.newName, this.state.newStops , this.state.newTimeStart , this.state.newTimeStop, this.state.newId);
                //console.log("1 true", this.props)
                this.props.getCrossRoutes(); //console.log("2 true", this.props)
                this.props.getRoutesBlank(); //console.log("3 true", this.props)
                
                this.props.routeHaveStops(); //console.log("4 true")
                this.props.getStopsBlank(); //console.log("5 true")
                this.props.getTransportsRoute(); //console.log("6 true")
                this.props.getTransportsBlank(); //console.log("7 true")
                this.props.getHomeData(); //console.log("8 true")
                this.props.getHomePageBlank(); //console.log("9 true")
                
                this.props.getRoutePage(this.props.activePage)
                
                this.changeisAdding()
        } else {
                this.setState({isAdding: true, isWrong: true, isSuccess: false, isFieldsHave: false});
        }
            
    }
    handleAddStop (event) {
        event.preventDefault();
        this.setState({newStops: [...this.state.newStops, null]});
            
    }


    render() {
        //console.log("PROPS", this.props)
        
        return (
            <div>
                <Search 
                nameSearch="Search for routes"
                    fields={[
                        {label:'Route', type:"search", placeholder:"", className:"input input-search", id:"routeName"},
        			    {label:'Сross paths', type:"search", placeholder:"", className:"input input-search", id:"routeCross"},
                    ]}
                    updateData={this.updateDataRoute}
                />
      {this.props.user && this.props.user.isAdmin ? 
    <div className="container"> 
        <div className="card card-search">
             <div className="row margin-edit margin-edit-top">
                <button className="button-add" onClick={this.changeisAdding}>{ this.state.isEditting ? 'Edit the route' : 'Add a new route' }</button>
             </div>
             
             
              {this.state.isAdding ? 
              // this.state.newNumber && this.state.newType && this.state.newPlace && this.state.newRoute 
            <div className="">
                 <div className="edit-data">
                    <form className="form-add flex-column" onSubmit={this.handleSubmit}>
                    {!this.state.isFieldsHave ? 
        			<div className="row wrong">
        				<h3>Not all fields are filled</h3>
        			</div>
        			: ''}
        			
                <div className="row">
                    <div className="element-edit">
    	                <div className="label margin-edit">Name</div>
    	                {/*<div className="value">*/}
    	                    <input 
    	                        type="text" 
    	                        placeholder="Name" 
    	                        className="input input-edit" 
    	                        id="newRouteName" 
    	                        value={this.state.newName}
    	                        onChange={this.handleChangeNewName}
	                        />
    	                {/*</div>>*/}
	                </div>  
	                
                    <div className="element-edit">
    	                <div className="label margin-edit">Time Start</div>
    	                {/*<div className="value">*/}
    	                    <input 
    	                        type="time" 
    	                        placeholder="Type" 
    	                        className="input input-edit" 
    	                        id="newRouteTimeStart" 
    	                        value={this.state.newTimeStart}
    	                        onChange={this.handleChangeNewTimeStart}
	                        />
    	                {/*</div>>*/}
	                </div> 
                    <div className="element-edit">
    	                <div className="label margin-edit">Time Stop</div>
    	                {/*<div className="value">*/}
    	                    <input 
    	                        type="time" 
    	                        placeholder="Time Stop" 
    	                        className="input input-edit" 
    	                        id="newRouteTimeStop" 
    	                        value={this.state.newTimeStop}
    	                        onChange={this.handleChangeNewTimeStop}
	                        />
    	                {/*</div>*/}
	                </div>  
	           </div>
	                <div className="row edit-add margin-edit">
    	                <div className="element-button">
        	                {/*<div className="value">*/}
        	                    <button className="button-add" id="addStop"  onClick={this.handleAddStop}>Add Stop</button>
        	                {/*</div>*/}
    	                </div> 
    	                
    	                { this.state.newStops.map((newStop, i) => {
    	                    return (<div className="element-button">
            	                <div className="label">Stop # {i+1}</div>
            	                {/*<div className="value">*/}
            	                    <select className="select-edit" name={i} value={newStop} onChange={this.handleChangeNewStops}>
            	                    <option value={-1} disabled selected>Select stop # {i+1}</option>
                	                { 
                	                this.props.json.stops.map(stop => {
                	                    return (<option value={stop.id}>{stop.name}</option>)
                	                })
                	                 
            	                }
                	                </select>
                	                <a className="button-cursor delete-data" onClick={this.deleteStop(i)}>Delete</a>
            	                {/*</div>*/}
        	                </div>)  
    	                })
                        
    	                }
    	                <div className="element-button edit-add">
        	                <div className="value">
        	                    <button className="button-add" id="routeAddEnter"  onSubmit={this.handleSubmit}>{ this.state.isEditting ? 'Edit' : 'Add' }</button>
        	                </div>
    	                </div>
    	            </div>
	                </form>
                </div>
             </div>
             : ''}
        </div>
    </div>
    : ''}           
        <div className="container">
            <div className="card card-list">
                {
                this.props.routesPage && this.props.routesPage.length ?
                    this.props.routesPage.map(route => {
                        return <div className="row">
                                    <div className="element">
                                	    <p className="label">Route</p>
                                		<p className="value">
                                		    <NavLink to={'/routes/'+route.id}> {route.name} </NavLink>
                            		    </p>
                                    </div>
                                    <div className="element">
                                	    <p className="label">Cross path</p>
                                		<p className="value list">
                                	    {
                                	        route.crossRoutesObj ? route.crossRoutesObj.map(crossRoute => {
                                	           return (
                                	            <NavLink to={'/routes/'+crossRoute.id}> {crossRoute.name}  </NavLink>
                                	        )}) : ''
                                		}
                                	    </p>
                                    </div>
                                    {this.props.user && this.props.user.isAdmin ? 
                                    <div className="element">
                                	    <p className="label">Actions</p>
                                	    <p className="value">
                                            <button onClick={this.editRow(route)}>Edit</button>
                                            <button className="button-delete" onClick={this.deleteRow(route)}>Delete</button>
                                	    </p>
                                    </div>
                                : '' }
                                </div>
                            })
                        : <div className="no-elements"><p>No results found</p></div>
                       
                }
                
                
                
            </div>
        </div>
        <div className="container">
            <div class="pagination">
        {/* {console.log('totalPages', this.props.totalPages)} */}
        { [...Array(this.props.totalPages).keys()].map(i=> {
            return <button onClick={this.pageChange(i)} className={ i === this.props.activePage ?'active': ''}> {i+1} </button>
        })
        }
        </div>
        </div>
    </div>
        );
    }
}


const mapStateToProps = store => {
  //console.log("RouteListPage", store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
       
        getRoutesFilter: (name, route, sort) => dispatch(getRoutesFilter(name, route, sort)),
        getRoutePage: pageNumber => dispatch(getRoutePage(pageNumber)),
        addRowRoute:  (newName, newStops, newTimeStart, newTimeStop, id) => dispatch(addRowRoute(newName, newStops, newTimeStart, newTimeStop, id)),
        deleteRowRoute: route => dispatch(deleteRowRoute(route)),
        
        getCrossRoutes: () => dispatch(CrossRoutes()),
        getRoutesBlank: () => dispatch(getRoutesBlank()),
        routeHaveStops: () => dispatch(routeHaveStops()),
        getStopsBlank: () => dispatch(getStopsBlank()),
        getTransportsRoute: () => dispatch(getTransportsRoute()),
        getTransportsBlank: () => dispatch(getTransportsBlank()),
        getHomeData: () => dispatch(getHomeData()),
        getHomePageBlank: () => dispatch(getHomePageBlank()),
            
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RouteListPage))
//export default RouteListPage