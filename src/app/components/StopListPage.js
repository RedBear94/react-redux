import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { getStopsFilter, getStopPage, addRowStop, deleteRowStop, fetchPointsFromServer} from '../reducers'
import { CrossRoutes, getRoutesBlank,  routeHaveStops, getStopsBlank, getTransportsRoute, getTransportsBlank, getHomeData, getHomePageBlank} from '../reducers'

class StopListPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        //console.log('TTT', this.props.routeHaveStops)
        //this.props.getrouteHaveStops()
        //!this.props.routeHaveStops.length ? this.props.getrouteHaveStops() : ''
        //console.log("!!!!!!!!!!!!!!!!!!!!!!",this.props.routeHaveStops)
        !this.props.routeHaveStops.length ? this.props.routeHaveStops() : ''
        !this.props.stops.length ? this.props.getStopsBlank() : ''
        //this.props.getStopsBlank()
        
        this.updateDataStop = this.updateDataStop.bind(this);
        this.state = {
            stopName: '',
            stopRoute: '',
            sort: '',
            page: 0,
            isAdding: false,
            }
        this.props.getStopPage(this.state.page)
        
        this.changeisAdding = this.changeisAdding.bind(this);
        this.handleChangeNewName = this.handleChangeNewName.bind(this)
        this.handleChangeNewX = this.handleChangeNewX.bind(this)
        this.handleChangeNewY = this.handleChangeNewY.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        
        this.deleteRow = this.deleteRow.bind(this)
        this.editRow = this.editRow.bind(this)
        this.getPoints = this.getPoints.bind(this)
        this.setPoints = this.setPoints.bind(this)
        
    }

    deleteRow (stop) { 
         return () => {
            //console.log("StopDelete", stop);
            this.props.deleteRowStop( stop )
            
            this.props.routeHaveStops();
            this.props.getStopsBlank();
            this.props.getCrossRoutes();
            this.props.getRoutesBlank(); 
            this.props.getTransportsRoute();
            this.props.getTransportsBlank();
            this.props.getHomeData();
            this.props.getHomePageBlank();
            this.props.getStopPage(this.props.activePage)
         }
        }
        

     editRow (stop) { 
         return () => {
            //console.log("StopEdit", stop);
            const stopObj = this.props.json.stops.find(r => r.id == stop.id)
            this.setState({
                isAdding: true,
                isEditting: true,
                isFieldsHave: true,
                newId: stopObj.id,
                newName: stopObj.name,
                newX: stopObj.point.x,
                newY: stopObj.point.y,
                //newName, newX, newY, id
            })
         }
        }
    
     updateDataStop  (value) { // {id: 'stopName', value: 'jhjhvjhjh'}
            //console.log("StopList", value)
            const t = this.state; // old
            t[value.id] = value.value;
            this.setState({[value.id]: value.value}) 
            //console.log("t", t)
            this.props.getStopsFilter(t.stopName, t.stopRoute, t.sort)
            this.props.getStopPage(0)
            
            // this.setState((prevState) => {
            //     return {value: prevState.value}});
            // this.props.getStopsFilter(value.name, value.route, value.sort)
        }
        
     pageChange (pageNumber) { 
         return () => {
            //console.log("pageNumber", pageNumber)
            this.setState({page: pageNumber})
            this.props.getStopPage(pageNumber)
        }
    }
    
    changeisAdding(){
       this.setState(prevState => ({
            newId: null, 
            newName: null,
            newX:  null, 
            newY: null,
            isEditting: false,
            isFieldsHave: true, 
            isAdding: !prevState.isAdding
        }))
    }
    handleChangeNewName(event) {
        this.setState({newName: event.target.value});
    }
    handleChangeNewX(event) {
        this.setState({newX: event.target.value});
    }
    handleChangeNewY(event) {
        this.setState({newY: event.target.value});
    }
    handleSubmit (event) {
        event.preventDefault();
        if (this.state.newName && this.state.newX && this.state.newY ){
            //console.log('form is submitted');
            // const user = this.props.json.users.find(user => user.email == this.state.email )
            // console.log('user', user)
            // if (user)
            //     this.setState({isReg: false, isPasswordWrong: false, isEmailHas: true, isFieldsHave: true});
            // else if (this.state.password !==this.state.password2)
            //     this.setState({isReg: false, isPasswordsWrong: true, isEmailHas: false, isFieldsHave: true});
            // else {
                this.setState({isAdding: true, isSuccess: true, isWrong: false,  isFieldsHave: true});
                this.props.addRowStop(this.state.newName, this.state.newX, this.state.newY, this.state.newId);
                this.props.routeHaveStops();
                this.props.getStopsBlank();
                
                this.props.getCrossRoutes();
                this.props.getRoutesBlank();
                this.props.getTransportsRoute();
                this.props.getTransportsBlank();
                this.props.getHomeData();
                this.props.getHomePageBlank();
                
                this.props.getStopPage(this.props.activePage)
                
                
                this.changeisAdding()
            // }
        } else {
                this.setState({isAdding: true, isWrong: true, isSuccess: false, isFieldsHave: false});
        }
            
    }
    
    getPoints () {
        this.props.fetchPointsFromServer(this.state.newName); 
        
    }
    
    setPoints () {
        const point = this.props.points;
        //console.log('getPoints++', );
        this.setState({newX: point.x, newY: point.y })
    }
    
    render() {
        return (
            <div>
                <Search 
                nameSearch="Search for stops"
                    fields={[
                        {label:'Stop', type:"search", placeholder:"", className:"input input-search", id:"stopName"}, //stopName
        			    {label:'Routes', type:"search", placeholder:"", className:"input input-search", id:"stopRoute"}, //stopRoute
                    ]}
                    updateData={this.updateDataStop}
                />
                
    { this.props.user && this.props.user.isAdmin ?
    
    <div className="container"> 
        <div className="card card-search">
             <div  className="row margin-edit margin-edit-top">
                <button className="button-add" onClick={this.changeisAdding}>{ this.state.isEditting ? 'Edit the stop' : 'Add a new stop' }</button>
             </div>
             
             
              {this.state.isAdding ? 
                 <div className="edit-data">
                    <form className="form-add flex-column" onSubmit={this.handleSubmit}>
                    {!this.state.isFieldsHave ? 
        			<div className="row wrong">
        				<h3>Not all fields are filled</h3>
        			</div>
        			: ''}
                <div className="row ">
                    <div className="element-edit">
    	                <div className="label margin-edit">Name</div>
    	                <div className="value">
    	                    <input 
    	                        type="text" 
    	                        placeholder="Name" 
    	                        className="input input-edit" 
    	                        id="newStopName" 
    	                        value={this.state.newName}
    	                        onChange={this.handleChangeNewName}
	                        />
	                      </div>  
	                      </div>  
                     
                     
                     <div className="element-button yandex-margin-top">
    	                <div className="label margin-edit"><span>Getting points from Yandex</span></div>
    	                <div className="value margin-edit">
	                        <a onClick={this.getPoints} className="button-add button-cursor">Get points from Yandex</a>
    	                </div>
	                </div>  
	                
	                
	                <div className="element-button yandex-margin-top">
    	                <div className="label margin-edit">{this.props.points ? <span>Coordinates: {this.props.points.x} {this.props.points.y} </span>: '' } </div>
    	                <div className="value margin-edit">
    	                    {this.props.points ? <a className="button-add button-cursor" onClick={this.setPoints}>Copy to fields</a> : '' }
    	                 </div>  
	                </div>
	                
	                 
	                </div> 
	              <div className="row ">  
                    <div className="element-edit">
    	                <div className="label margin-edit">X</div>
    	                <div className="value">
    	                    <input 
    	                       // type="text" 
    	                        type="number" 
    	                        step="any"
    	                        placeholder="X" 
    	                        className="input input-edit" 
    	                        id="newStopX" 
    	                        value={this.state.newX}
    	                        onChange={this.handleChangeNewX}
	                        />
    	                </div>
	                </div> 
                    <div className="element-edit">
    	                <div className="label margin-edit">Y</div>
    	                <div className="value">
    	                    <input 
    	                       // type="text" 
    	                        type="number" 
    	                        step="any"
    	                        placeholder="Y" 
    	                        className="input input-edit" 
    	                        id="newStopY" 
    	                        value={this.state.newY}
    	                        onChange={this.handleChangeNewY}
	                        />
    	                </div>
	                </div>
	           </div>
	           <div className="row edit-add margin-edit">
	                <div className="element-button">
    	                <div className="value">
    	                    <button className="button-add" id="stopAddEnter"  onSubmit={this.handleSubmit}>{ this.state.isEditting ? 'Edit' : 'Add' }</button>
    	                </div>
	                </div> 
	           </div>
	           
	                </form>
                </div>
             
             : ''}
        </div>
    </div>
    : ''}
                
    <div className="container">
        <div className="card card-list">
            
            {
            this.props.stopsPage && this.props.stopsPage.length ?
                this.props.stopsPage.map(stop => {
                    return <div className="row">
                                <div className="element">
                            	    <p className="label">Stop name</p>
                            		<p className="value">
                            		    <NavLink to={'/stops/'+stop.id}> {stop.name} </NavLink>
                        		    </p>
                                </div>
                                <div className="element">
                            	    <p className="label">Route</p>
                            		<p className="value list">
                            	    {
                            	        stop.routes ? stop.routes.map(route => {
                            	           return (
                            	            <NavLink to={'/routes/'+route.id}> {route.name}  </NavLink>
                            	        )}) : ''
                            		}
                            	    </p>
                                </div>
                                
                                {this.props.user && this.props.user.isAdmin ? 
                                    <div className="element">
                                	    <p className="label">Actions</p>
                                	    <p className="value">
                                            <button onClick={this.editRow(stop)}>Edit</button>
                                            <button className="button-delete" onClick={this.deleteRow(stop)}>Delete</button>
                                	    </p>
                                    </div>
                                : '' }
                                
                            </div>
                        })
                    : <div className="no-elements"><p>No results found</p></div>
                }
        </div>
    </div>
        <div className="container">
        <div className="pagination">
        {/* {console.log('totalPages', this.props.totalPages)} */}
        { [...Array(this.props.totalPages).keys()].map(i=> {
            return <button onClick={this.pageChange(i)} className={ i === this.props.activePage ?'active': ''}> {i+1} </button>
        })
        }
        </div>
        </div>
</div>           
        );
    }
}

const mapStateToProps = store => {
  //console.log(store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        //getrouteHaveStops: StopRoutes => dispatch(routeHaveStops()),
        getStopsFilter: (name, route, sort) => dispatch(getStopsFilter(name, route, sort)),
        getStopPage: pageNumber => dispatch(getStopPage(pageNumber)),
        deleteRowStop: stop => dispatch(deleteRowStop(stop)),
        addRowStop:  (newName, newX, newY, id) => dispatch(addRowStop(newName, newX, newY, id)),
        fetchPointsFromServer:  address => dispatch(fetchPointsFromServer(address)),
        
           
        getCrossRoutes: () => dispatch(CrossRoutes()),
        getRoutesBlank: () => dispatch(getRoutesBlank()),
        routeHaveStops: () => dispatch(routeHaveStops()),
        getStopsBlank: () => dispatch(getStopsBlank()),
        getTransportsRoute: () => dispatch(getTransportsRoute()),
        getTransportsBlank: () => dispatch(getTransportsBlank()),
        getHomeData: () => dispatch(getHomeData()),
        getHomePageBlank: () => dispatch(getHomePageBlank()),
            
    }
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(StopListPage))
