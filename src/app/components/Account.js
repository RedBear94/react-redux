import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { fetchUserByEmailAndPassword } from '../reducers'
import { Route, Redirect } from 'react-router'

class AccountPage extends Component {
    render() {
        return (
            !this.props.user ? 
                    <Redirect to="/authorization" />
                :
                <div className="container">
                    <div className="card card-search">
                        <div className="row">
            				<h2>My account</h2>
            			</div>
                        <div className="row">
                            <div className="element">
            	                <div className="label">Name</div>
            	                <div className="value">{this.props.user.name}</div>
        	                </div>  
                            <div className="element">
            	                <div className="label">Email</div>
            	                <div className="value">{this.props.user.email}</div>
        	                </div>  
                            <div className="element">
            	                <div className="label">Is it admin</div>
            	                <div className="value">{this.props.user.isAdmin ? 'Yes' : 'No'}</div>
        	                </div>  
                        </div>
                    </div>
                </div>
            
        );
    }
}


const mapStateToProps = store => {
  //console.log(store) 
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUserByEmailAndPassword: (id, password) => dispatch(fetchUserByEmailAndPassword(id, password))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AccountPage))