//import { withRouter } from 'react-router-dom';
import React, { Component } from 'react'
import Navigation from './Navigation'
import Routing from './Routing'
import Footer from './Footer'
import { connect } from 'react-redux'
import { fetchAllItemsFromServer } from '../reducers'
import { combineReducers, createStore } from 'redux'
import { fetchRouteById } from '../reducers'

import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter
} from "react-router-dom";

class App extends Component {

    render() {
        const links = [
                  { id: 1, isActive: true, href: '/', title: 'Home'}, 
                  { id: 2, isActive: false, href: '/stops', title: 'Stops'}, 
                  { id: 3, isActive: false, href: '/routes', title: 'Routes'}, 
                  { id: 4, isActive: false, href: '/transports', title: 'Car park'}, 
                ]
        
        this.props.user ? links.push({ id: 6, isActive: false, href: '/account', title: 'Account'}) : '' 
        this.props.user ? links.push({ id: 7, isActive: false, href: '/logout', title: 'Log out'} ) : '' 
        !this.props.user ? links.push({ id: 5, isActive: false, href: '/authorization', title: 'Log in'})  : '' 
            
            
        return (
            <div>
            
            <Navigation links={links} /> 
            
              { this.props.isDownloaded ? 
                  <Routing />
               : 
                  <div className="container"><img className="loader" src="/static-assets/img/Blocks-1s-200px.svg" alt="Downloading..." /></div> 
              }
              <Footer isUser={!!this.props.user} />
              {/*<button onClick={this.context.router.history.goBack}>Back</button>*/}
              
            </div>
        );
    }
}

const mapStateToProps = store => {
  //console.log('storeApp', store) 
  return store
}

export default withRouter(connect(mapStateToProps)(App))