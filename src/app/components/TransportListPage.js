import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { getTransportsRoute, getTransportsBlank,  getTransportsFilter, deleteRowTransport, getTransportPage, addRowTransport}  from '../reducers'

class TransportListPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        !this.props.transportsRoute.length ? this.props.getTransportsRoute() : ''//console.log('getTransportsRoute')
        !this.props.transports.length ? this.props.getTransportsBlank() : ''
        
        this.updateDataTransport = this.updateDataTransport.bind(this);
        this.deleteRow = this.deleteRow.bind(this);
        this.pageChange = this.pageChange.bind(this);
        
        this.state = {
            transportNumber: '',
            transportType: '',
            transportRoute: '',
            transportSeats: '',
            sort: '',
            page: 0,
            isAdding: false,
            isEditting: false,
            isFieldsHave: true,
            //totalPages: getTotalPageCount(this.props.transports.length, countRows),
            }
        //console.log("counts", this.props.transports, countRows,  getTotalPageCount(this.props.transports.length, countRows) )
        this.props.getTransportPage(this.state.page)
        
        
        this.editRow = this.editRow.bind(this);
        this.changeisAdding = this.changeisAdding.bind(this);
        this.handleChangeNewNumber = this.handleChangeNewNumber.bind(this)
        this.handleChangeNewType = this.handleChangeNewType.bind(this)
        this.handleChangeNewPlaces = this.handleChangeNewPlaces.bind(this)
        this.handleChangeNewRoute = this.handleChangeNewRoute.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    
    
     updateDataTransport  (value) { 
            //console.log("TransportList", value)
            const t = this.state; // old
            t[value.id] = value.value;
            this.setState({[value.id]: value.value}) 
            this.props.getTransportsFilter( t.transportNumber, t.transportType, t.transportRoute, t.transportSeats,  t.sort)
            this.props.getTransportPage(0)
        }

     deleteRow (transport) { 
         return () => {
            //console.log("TransportDelete", transport);
            this.props.deleteRowTransport( transport )
            this.props.getTransportPage(this.props.activePage)
         }
        }
        

     editRow (transport) { 
         return () => {
            //console.log("TransportEdit", transport);
            this.setState({
                isAdding: true,
                isEditting: true,
                newNumber: transport.number, 
                newType: transport.type,
                newPlaces: transport.places, 
                newRoute: transport.route,
            })
            
            // this.props.deleteRowTransport( transport )
            // this.props.getTransportPage(this.props.activePage)
         }
        }
        
     pageChange (pageNumber) { 
         return () => {
            //console.log("pageNumber", pageNumber)
            this.setState({page: pageNumber})
            this.props.getTransportPage(pageNumber)
        }
    }
    
    changeisAdding(){
        this.setState(prevState => ({
            newNumber: null, 
            newType: null,
            newPlaces:  null, 
            newRoute: null,
            isEditting: false,
            isAdding: !prevState.isAdding
        }))
    }
    handleChangeNewNumber(event) {
        this.setState({newNumber: event.target.value});
    }
    handleChangeNewType(event) {
        this.setState({newType: event.target.value});
    }
    handleChangeNewPlaces(event) {
        this.setState({newPlaces: event.target.value});
    }
    handleChangeNewRoute(event) {
        this.setState({newRoute: event.target.value});
    }
    
    handleSubmit (event) {
        event.preventDefault();
        //console.log('handleSubmit', this.state.newNumber, this.state.newType , this.state.newPlaces , this.state.newRoute)
        if (this.state.newNumber && this.state.newType && this.state.newPlaces && this.state.newRoute ){
            //console.log('form is submitted');

                this.setState({isAdding: true, isSuccess: true, isWrong: false,  isFieldsHave: true});
                this.props.addRowTransport(this.state.newNumber, this.state.newType, this.state.newPlaces, this.state.newRoute);
                this.props.getTransportsRoute();
                this.props.getTransportsBlank();
                this.props.getTransportPage(this.props.activePage)
                
                this.changeisAdding()
        } else {
                this.setState({isAdding: true, isWrong: true, isSuccess: false, isFieldsHave: false});
        }
            
    }
    
    
    render() {
        return (
    <div>
        <Search 
            nameSearch="Search for transports"
            fields={[
                {label:'Number', type:"search", placeholder:"", /*value:this.state.transportNumber, */ className:"input input-search", id:"transportNumber"},
            	{label:'Type', type:"search", placeholder:"", className:"input input-search", id:"transportType"},
            	{label:'Route', type:"search", placeholder:"", className:"input input-search", id:"transportRoute"},
                {label:'Number of seats', type:"search", placeholder:"", className:"input input-search", id:"transportSeats"}
                ]}
            updateData={this.updateDataTransport}
        />
         
         
 { this.props.user && this.props.user.isAdmin ? 
    <div className="container"> 
        <div className="card card-search">
             <div className="row margin-edit margin-edit-top">
                <button className="button-add" onClick={this.changeisAdding}>{ this.state.isEditting ? 'Edit the transport' : 'Add a new transport' }</button>
             </div>
             
             
              {this.state.isAdding ? 
              // this.state.newNumber && this.state.newType && this.state.newPlace && this.state.newRoute 
                 <div className="edit-data">
                    <form className="form-add flex-column" onSubmit={this.handleSubmit}>
                    {!this.state.isFieldsHave ? 
        			<div className="row wrong">
        				<h3>Not all fields are filled</h3>
        			</div>
        			: ''}
                <div className="row">
                    <div className="element-edit">
    	                <div className="label margin-edit">Number</div>
    	                {/*<div className="value">*/}
    	                    <input 
    	                        type="text" 
    	                        placeholder="Number" 
    	                        className="input input-edit"
    	                        id="newTransportNumber" 
    	                        value={this.state.newNumber}
    	                        onChange={this.handleChangeNewNumber}
    	                        disabled={this.state.isEditting ? true: false }
	                        />
    	                {/*</div>*/}
	                </div>  
	                
                    <div className="element-edit">
    	                <div className="label margin-edit">Type</div>
    	                {/*<div className="value">*/}
    	                    <input 
    	                        type="text" 
    	                        placeholder="Type" 
    	                        className="input input-edit" 
    	                        id="newTransportType" 
    	                        value={this.state.newType}
    	                        onChange={this.handleChangeNewType}
	                        />
    	                {/*</div>*/}
	                </div> 
                    <div className="element-edit">
    	                <div className="label margin-edit">Places</div>
    	                {/*<div className="value">*/}
    	                    <input 
    	                       // type="text" 
    	                        type="number" 
    	                        step="1"
    	                        placeholder="Places" 
    	                        className="input input-edit" 
    	                        id="newTransportPlaces" 
    	                        value={this.state.newPlaces}
    	                        onChange={this.handleChangeNewPlaces}
	                        />
    	                {/*</div>*/}
	                </div>
	            </div>
	            <div className="row edit-add margin-edit">
                    <div className="element-button ">
    	                <div className="label">Route</div>
    	                {/*<div className="value">*/}
    	                    <select className="select-edit" value={this.state.newRoute} onChange={this.handleChangeNewRoute}>
    	                    
    	                    <option value={-1} disabled selected={ !this.state.newRoute ? 'selected': false }>Select Route</option>
    	                    
        	                { this.props.json.routes.map(route => {
        	                    //console.log(route.id, this.state.newRoute)
        	                    return (<option value={route.id} selected={ route.id==this.state.newRoute ? 'selected': false }>{route.name}</option>)
        	                })
    	                    }
        	                </select>
    	                {/*</div>*/}
	                </div>  
	                <div className="element-button edit-add">
    	                <div className="value">
    	                    <button className="button-add" id="stopAddEnter"  onSubmit={this.handleSubmit}>{ this.state.isEditting ? 'Edit' : 'Add' }</button>
    	                </div>
	                </div> 
	           </div>
	                </form>
                </div>
             : ''}
        </div>
    </div>
    : ''}
    
    
        <div className="container">
            <div className="card card-list">
                {
                    this.props.transportsPage && this.props.transportsPage.length ?
                         this.props.transportsPage.map(transport => {
                    
                    return <div className="row">
                                <div className="element">
                            	    <p className="label">Number</p> 
                            	    <p className="value important">
                            	        <NavLink to={'/transports/'+transport.number}>{transport.number}</NavLink>
                        	        </p>
                            	</div>
                            	
                            	<div className="element">
                                    <p className="label">Type</p> 
                            		<p className="value">
                        		        {transport.type}
                        		     </p>
                            	</div>
                            	
                            	<div className="element">
                            	    <p className="label">Route</p>
                            		<p className="value">
                            		{transport.routeObj ? 
                            		    <NavLink to={'/routes/'+transport.routeObj.id}>
                            	            {transport.routeObj.name}
                            	   </NavLink>
                            	   : '-'
                            	   }
                            	   
                        	        </p>
                                </div>
                                
                                <div className="element">
                            	    <p className="label">Number of seats</p>
                            	    <p className="value">
                                        {transport.places}
                            	    </p>
                                </div>
                                
                                {this.props.user && this.props.user.isAdmin ? 
                                    <div className="element">
                                	    <p className="label">Actions</p>
                                	    <p className="value">
                                            <button onClick={this.editRow(transport)}>Edit</button>
                                            <button className="button-delete" onClick={this.deleteRow(transport)}>Delete</button>
                                	    </p>
                                    </div>
                                : '' }
                            </div>
                        })
                    : <div className="no-elements"><p>No results found</p></div>
                }
                
            </div>
        </div>
        <div className="container">
            <div class="pagination">
        {/* {console.log('totalPages', this.props.totalPages)} */}
        { [...Array(this.props.totalPages).keys()].map(i=> {
            return <button onClick={this.pageChange(i)} className={ i === this.props.activePage ?'active': ''}> {i+1} </button>
        })
        }
        </div>
        </div>
    </div>
        );
    }
}

const mapStateToProps = store => {
  //console.log(store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTransportsRoute: () => dispatch(getTransportsRoute()),
        getTransportsBlank: () => dispatch(getTransportsBlank()),
        getTransportsFilter: (transportNumber, transportType, transportRoute, transportSeats, filter) => dispatch(getTransportsFilter(transportNumber, transportType, transportRoute, transportSeats, filter)),
        deleteRowTransport: transport => dispatch(deleteRowTransport(transport)),
        getTransportPage: pageNumber => dispatch(getTransportPage(pageNumber)),
        addRowTransport:  (newNumber, newType, newPlaces, newRoute) => dispatch(addRowTransport(newNumber, newType, newPlaces, newRoute)),
    }
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TransportListPage))
//export default TransportListPage