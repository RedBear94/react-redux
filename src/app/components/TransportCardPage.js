import React, { Component } from 'react'
import Card from './Card'

import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';

import { fetchTransportByName } from '../reducers'

class TransportCardPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        this.state = {id: props.match.params.id}
        
        this.props.getTransportByName(this.state.id)
    }
    render() {
            return (
            <div>
                { this.props.isDownloaded  ? 
                <div>
                    {/*{ console.log('routeCardPageProps', this.props) }*/}
                    {/*{ console.log('routeCardPageState', this.state) }*/}
                    <Card  
                            title="Transport"
                            name= {this.props.route.number}
                            points= {this.props.route.stopsList}
                            // points= {[
                            //     { name: 'Проспект Героев', point: [59.859217, 30.155995]},
                            //     { name: 'Улица Доблести', point: [59.859962, 30.178426]},
                            //     { name: 'Проспект  Кузнецова', point: [59.856863, 30.186717]},
                            //     { name: 'Улица Десантников', point: [59.854097, 30.201090]},
                            //     { name: 'Проспект Маршала Жукова', point: [59.849790, 30.218527]},
                            //     ]}
                        />
                </div>
                    : <p> Данные загружаются...</p>
                }
            </div>
        );
    }
}

const mapStateToProps = store => {
  //console.log('mapStateToProps', store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTransportByName: id => dispatch(fetchTransportByName(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TransportCardPage)
//export default TransportCardPage