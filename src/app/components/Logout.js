import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { fetchUserLogout } from '../reducers'
import { Route, Redirect } from 'react-router'

class LogoutPage extends Component {
    constructor (props) {
        super(props);
        props.fetchUserLogout();
    }
    
    render(){
        return <Redirect to="/" />
    }
    
}


const mapStateToProps = store => {
  //console.log(store) 
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUserLogout: () => dispatch(fetchUserLogout())
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LogoutPage))