import React, { Component } from 'react'
import SearchField from './SearchField'

class Search extends Component {
    constructor (props) {
        super(props);
        this.state ={
          updateData : props.updateData,
          value: ''
        };
        this.updateData = this.updateData.bind(this);
    }   
    
    // updateData (fieldId){
    //     return function (value) {
    //         console.log(value)
    //         this.state.updateData(fieldId, value)
    //     }
    // }
    
     updateData (value) {
            //console.log("Search",value)
            this.state.updateData(value)
    }
    
    render() {
        return (
          <section className="search">
        	<div className="container">
        		<div className="card card-search">
        			<div className="row">
        				<h2>{this.props.nameSearch}</h2>
        			</div>
        			
        			<div className="row">
        			{
        			this.props.fields.map( field => {
                        return <SearchField {...field} key={field.id} updateDataField={this.updateData} /> 
                        })
        			}
        			
        			     
        			</div>
        		</div>
        	</div>
        </section>
        );
    }
}


export default Search