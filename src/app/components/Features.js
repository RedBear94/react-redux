import React, { Component } from 'react'

class Features extends Component {
    render() {
        return (
	 <section className="container">
			<div className="card">
        		<div className="border-for-all info row">
        			<div className="div_margin div-size-1">
        				<img src="/static-assets/img/car1.png" alt="" className="car-icons" />
        				<p>Our system covers different types of transport</p>
        			</div>
        			<div className="div_margin div-size-1">
        				<img src="/static-assets/img/car2.png" alt="" className="car-icons" />
        				<p>Quick registration</p>
        			</div>
        			<div className="div_margin div-size-1">
        				<img src="/static-assets/img/car3.png" alt="" className="car-icons" />
        				<p>Convenient database for searching the required routes</p>
        			</div>
        		</div>
    		</div>
    	</section>
	
        );
    }
}


export default Features