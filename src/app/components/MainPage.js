import React, { Component } from 'react'
import Welcome from './Welcome'
import Search from './Search'
import Features from './Features'
import News from './News'
import Reviews from './Reviews'

import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { getHomeData, getHomePageBlank, getHomePageFilter, getHomePage} from '../reducers'

class MainPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        
        // this.props.getHomeData()
        // this.props.getHomePageBlank()
        !this.props.homePageData.length ? this.props.getHomeData() : ''
        !this.props.searchdata.length ? this.props.getHomePageBlank() : ''
        
        //this.props.getHomePageFilter("Муниципальный округ Кронверкское", "Улица Доблести", "")
        this.updateDataHome = this.updateDataHome.bind(this);
        this.state = {
            homeStop1: '',
            homeStop2: '',
            homeTime: '',
            sort: '',
            page: 0,
            }
        this.props.getHomePage(this.state.page)
    }
    
    updateDataHome  (value) { 
            //console.log("HomePageList", value)
            const t = this.state; // old
            t[value.id] = value.value;
            this.setState({[value.id]: value.value}) 
            this.props.getHomePageFilter( t.homeStop1, t.homeStop2, t.homeTime)
            this.props.getHomePage(0)
        }
        
    pageChange (pageNumber) { 
         return () => {
            //console.log("pageNumber", pageNumber)
            this.setState({page: pageNumber})
            this.props.getHomePage(pageNumber)
        }
    }
        
    render() {
        return (
            <main>
                <Welcome/>
                <Search
                    nameSearch="Search for routes"
                    fields={[
                        {label:'Departure point', type:"search", placeholder:"Departure point", className:"input input-search", id:"homeStop1"},
        			    {label:'Place of arrival', type:"search", placeholder:"Place of arrival", className:"input input-search", id:"homeStop2"},
        			    {label:'Beginning of work', type:"time", min:"00:00", max:"23:59", className:"input input-time", id:"homeTime" }
                    ]}
                    updateData={this.updateDataHome}
                />
                
    <div className="container">
        <div className="card card-list">
            
            {
            this.props.homesPage && this.props.homesPage.length ?
                this.props.homesPage.map(route => {
                    return <div className="row">
                                <div className="element">
                            	    <p className="label">Route Name</p>
                            		<p className="value">
                            		    <NavLink to={'/routes/'+route.id}> {route.name} </NavLink>
                        		    </p>
                                </div>
                                <div className="element">
                            	    <p className="label">Stops</p>
                            		<div className="value">
                            	    {
                            	        route.stopsObj ? route.stopsObj.map(stop => {
                            	           return (
                            	            <p><NavLink to={'/stops/'+stop.id}> {stop.name}  </NavLink></p>
                            	        )}) : ''
                            		}
                            	    </div>
                                </div>
                                <div className="element">
                            	    <p className="label">Time</p>
                            		<p className="value">
                            		    {route.time_start} - {route.time_stop}
                        		    </p>
                                </div>
                            </div>
                        })
                    : <div className="no-elements"><p>No results found</p></div>
                }
        </div>
    </div>
        
        <div className="container">
            <div class="pagination">
        {/* {console.log('totalPages', this.props.totalPages)} */}
        { [...Array(this.props.totalPages).keys()].map(i=> {
            return <button onClick={this.pageChange(i)} className={ i === this.props.activePage ?'active': ''}> {i+1} </button>
        })
        }
        </div>
        </div>
                
                <Features/>
                {/*
                <section className="container">
            		<div className="card">
                		<div className="container border-for-all main-text">
                            <News/>
                            <Reviews/>
                        </div>
                    </div>
                </section>
                */}
            </main>
        );
    }
}



const mapStateToProps = store => {
  //console.log(store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        getHomeData: () => dispatch(getHomeData()),
        getHomePageBlank: () => dispatch(getHomePageBlank()),
        getHomePageFilter: (homeStop1, homeStop2, homeTime) => dispatch(getHomePageFilter(homeStop1, homeStop2, homeTime)),
        getHomePage: pageNumber => dispatch(getHomePage(pageNumber))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainPage))

//export default MainPage