import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { fetchUserByNameEmailPasswords } from '../reducers'
import { Route, Redirect } from 'react-router'

class RegistrationPage extends Component {
    constructor (props) {
        super(props);
        this.props = props;
        this.state = {
            name: '',
            email: '',
            password: '',
            password2: '',
            isReg: false,
            isPasswordsWrong: false,
            isEmailHas: false,
            isFieldsHave: true,
        }
        
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeName = this.handleChangeName.bind(this)
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        this.handleChangePassword2 = this.handleChangePassword2.bind(this)
        
    }
    
    handleSubmit (event) {
        event.preventDefault();
        if (this.state.name && this.state.email && this.state.password && this.state.password2 ){
            //console.log('form is submitted');
            const user = this.props.json.users.find(user => user.email == this.state.email )
            //console.log('user', user)
            if (user)
                this.setState({isReg: false, isPasswordWrong: false, isEmailHas: true, isFieldsHave: true});
            else if (this.state.password !==this.state.password2)
                this.setState({isReg: false, isPasswordsWrong: true, isEmailHas: false, isFieldsHave: true});
            else {
                this.setState({isReg: true});
                this.props.fetchUserByNameEmailPasswords(this.state.name, this.state.email, this.state.password, this.state.password2)
            }
        } else {
            
                this.setState({isReg: false, isPasswordWrong: false, isEmailHas: false, isFieldsHave: false});
        }
            
    }
    
    handleChangeName(event) {
        this.setState({name: event.target.value});
    }
    
    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    }
    
    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }
    
    handleChangePassword2(event) {
        this.setState({password2: event.target.value});
    }
    
    render() {
        return (
             <div className="container">

                <div className="card card-login">
                    <form onSubmit={this.handleSubmit}>
                   
            			
            			<div className="row">
            				<h2>Registration</h2>
            			</div>
            			
            			{this.state.isPasswordsWrong ? 
                			<div className="row wrong auth">
                				<h3>Passwords do not match</h3>
                			</div>
                			: ''}
            			
            			{this.state.isEmailHas ? 
                			<div className="row wrong auth">
                				<h3>User with this address is already registered</h3>
                			</div>
                			: ''}
            			
            			{!this.state.isFieldsHave ? 
                			<div className="row wrong auth">
                				<h3>Not all fields are filled</h3>
                			</div>
                			: ''}
            			 
                        {this.state.isReg && this.props.user ? 
            			
            			<Redirect to="/account" />
            			
            			:
            			
            			<div className="row auth">
            	            <div className="element">
            	                <div className="label">Name</div>
            	                <div className="value">
            	                    <input 
            	                        type="text" 
            	                        placeholder="Name" 
            	                        className="input input-reg input-search" 
            	                        id="regName" 
            	                        value={this.state.name}
            	                        onChange={this.handleChangeName}
            	                        
            	                        />
            	                </div>
        	                </div>   
        	                <div className="element">
            	                <div className="label">Email</div>
            	                <div className="value">
            	                    <input 
            	                        type="email" 
            	                        placeholder="Email" 
            	                        className="input input-reg input-search" 
            	                        id="regEmail" 
            	                        value={this.state.email}
            	                        onChange={this.handleChangeEmail}
            	                        />
            	                </div>
        	                </div>    
        	                <div className="element">
            	                <div className="label">Password</div>
            	                <div className="value">
            	                    <input 
            	                        type="password" 
            	                        placeholder="Password" 
            	                        className="input input-reg input-search"
            	                        id="regPass1" 
            	                        value={this.state.password}
            	                        onChange={this.handleChangePassword}
        	                        />
            	                </div>
        	                </div>   
        	                <div className="element">
            	                <div className="label">Repeat password</div>
            	                <div className="value">
            	                    <input 
            	                    type="password" 
                	                    placeholder="Repeat password"
                	                    className="input input-reg input-search" 
                	                    id="regPass2"
            	                        value={this.state.password2}
            	                        onChange={this.handleChangePassword2}
            	                    />
            	                </div>
        	                </div>    
        	                <div className="element">
            	                <div className="value">
            	                    <button id="regEnter"  onSubmit={this.handleSubmit}>Enter</button>
            	                </div>
        	                </div>    
        	                <div className="element">
            	                <div className="label">
            	                    Already have an account? <NavLink to="/authorization">Log in</NavLink> to your account.
                                </div>
        	                </div>   
            			</div>
                    }
        			</form>
                </div>
            </div>
        );
    }
}


const mapStateToProps = store => {
  //console.log(store) 
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUserByNameEmailPasswords: (name, email, password, password2) => dispatch(fetchUserByNameEmailPasswords(name, email, password, password2))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RegistrationPage))