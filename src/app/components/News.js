import React, { Component } from 'react'

class News extends Component {
    render() {
        return (
          	<div className="div_margin div-size-1">
    			<h3>News</h3>
    			<p>19.10.2018 in the morning hours the route of bus № 173 is changed in Vyborgsky district</p>
    			
    			<p>17.10.2018 a new bus stop is added to the route № 81</p>
    			
    			<p>From 06 till 09.10.2018 and from 20 till 28.10.2018 the routes for bus № 3, 16 are changed in admiralteysky district</p>
    			
    			<p>03.10.2018 the routes for buses № 31, 95, 114, 116, К-31, К-114, К-116, К-253 are changed</p>
			</div>
        );
    }
}


export default News