import React, { Component } from 'react'
import { BrowserRouter, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

class NavLogo extends Component {
    constructor (props) {
        super(props);
    }
    
    render() {
        return (
            <div className="logo">
				<NavLink to="/">
					<img src="/static-assets/img/car-logo.svg" alt="" />
				</NavLink>
			</div>
        );
    }
}

class NavElement extends Component {
    
    constructor (props) {
        super(props);
        this.state = {
            isActive: props.isActive,
            href: props.href,
            title: props.title
        }
    }
    
    render() {
        return (
    		<li className={{/*this.state.isActive ? 'active' : ''*/}}>
    		    <NavLink activeClassName="selected" exact to={this.state.href}>{this.state.title}</NavLink>
    		</li>
        );
    }    
}

class NavElements extends Component {
    constructor (props) {
        super(props);
        this.props = props,
        this.state = {
            isVisable: props.isVisable,
            // links: [
            //      { id: 1, isActive: true, href: '/', title: 'Home'}, 
            //      { id: 2, isActive: false, href: '/stops', title: 'The Stops'}, 
            //      { id: 3, isActive: false, href: '/routes', title: 'Routes'}, 
            //      { id: 4, isActive: false, href: '/transports', title: 'Car Park'}, 
            //      { id: 5, isActive: false, href: '/authorization', title: 'Sign in'}, 
            //     ]
                //if(this.props.user){
    			//this.setState({links[5].title: this.state.links[5].title = Sign out});}
        }
    }
    
    render() {
        return (
            	<nav className={this.props.isVisable ? 'visable': 'unvisable'} id="nav">
    				<ul>
    				{
                        this.props.links.map( el => {
                        return <NavElement key={el.id} isActive={el.isActive} href={el.href} title={el.title} /> 
                        })
    				}
    				   
    				</ul>
    			</nav>
        );
    }
}

class NavBurger extends Component {
    constructor (props) {
        super(props);
        this.props = props;
        this.state = {
            isOpen : true,
            updateDataIsOpen : props.updateDataIsOpen
        };
        this.burgerSwitch = this.burgerSwitch.bind(this);
    }
    
    burgerSwitch() {
        this.setState({isOpen: !this.state.isOpen});
        this.state.updateDataIsOpen(this.state.isOpen);
    }
    
    
    render() {
        const isOpen = this.state.isOpen;
        return (
            <nav>
				<ul>
					<li className="burger"><a href="javascript:void(0);" className="icon" onClick={this.burgerSwitch}><i className="fa fa-bars"></i></a></li>
				</ul>
			</nav>
        );
    }
}




class Navigation extends Component {
    constructor (props) {
        super(props);
        this.props = props;
        this.state = { isOpen: false }
        this.updateDataIsOpen = this.updateDataIsOpen.bind(this);
    }
    
    
    updateDataIsOpen (value) {
        //console.log(value)
        this.setState({ isOpen: value });
        //console.log("Update: "+ this.state.isOpen);
    }
  
    
    render() {
        return (
            <header className="header">
        		<div className="container">
        		    <NavLogo />
        			<NavBurger updateDataIsOpen={this.updateDataIsOpen} />
        			<NavElements isVisable={this.state.isOpen} links={this.props.links} />
        		</div>
            </header>
        );
    }
}
export default Navigation