import React, { Component } from 'react'
import { withRouter, NavLink } from 'react-router-dom';

class StopsList extends Component{
    constructor(props){
        super(props);
        //this.props=props
        this.state = {alph:  ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z']}
    }
    render() {
        return (
        <div>
            <h3>Stops list:</h3>
                {
                    this.props.points ?
                    this.props.points.map((el, i) => {
                        return <p><span className=
                        {'num ' + (i===0 ? 'num-first' : '') + (i===this.props.points.length-1 
                        ? 'num-last' : '') }>
                        {this.state.alph[i]}
                        </span><NavLink to={'/stops/'+el.id}>{el.name}</NavLink></p>
                    })
                    : <p>No stops</p>
                }
         </div>
        );
    }
}

export default StopsList