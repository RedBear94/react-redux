import React, { Component } from 'react'

class SearchField extends Component {
    constructor(props){
        super(props)
        this.state ={
          updateDataField : props.updateDataField,
          value: ''
        };
        this.handleChange = this.handleChange.bind(this);
        
    }
    
    handleChange(event) {
        const value = event.target.value;
        this.setState({value: value});
        //this.state.updateDataField({[this.props.id]: value})
        this.state.updateDataField({id: this.props.id, value: value})
        //console.log("SearchField", value)
    }
    
    render() {
        return (
        <div className="element">
    		<div className="label">{this.props.label}</div>
    		<div className="autocomplete value">
    		    <input { ...this.props} onChange={this.handleChange} />
    		</div>
	    </div> 
	);
    }
}

export default SearchField