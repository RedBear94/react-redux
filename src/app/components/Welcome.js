import React, { Component } from 'react'

class Welcome extends Component {
    constructor (props) {
        super(props);
    }
    
    render() {
        return (
           <div className="welcome">
            	<div className="container">
            		<div className="row">
            			<p className="hello-text">Welcome to the transport information system</p>
            		</div>
            	</div>
            </div>
        );
    }
}


export default Welcome