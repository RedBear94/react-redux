import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { fetchUserByEmailAndPassword } from '../reducers'
import { Route, Redirect } from 'react-router'

class AuthorizationPage extends Component {
    constructor (props) {
        super(props);
        this.props = props;
        this.state = {
            email: '',
            password: '',
            isAuth: false,
            isPasswordWrong: false
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        
    }
    
    handleSubmit (event) {
        event.preventDefault();
        //console.log('form is submitted');
        const user = this.props.json.users.find(user => user.email == this.state.email && user.password == this.state.password)
        //console.log('user', user)
        if (user){
            this.setState({isAuth: true, isPasswordWrong: false, user: user});
            this.props.fetchUserByEmailAndPassword(this.state.email, this.state.password)
        }
            
        else
            this.setState({isAuth: false, isPasswordWrong: true});
    }
    
    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    }
    
    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }
    
    render() {
        return (
            <div className="container">
                {/* { this.props.isDownloaded ? 
                console.log('users', this.props.json.users, 'users', this.props)
                : '' } */}
                <div className="card card-login">
                    <form onSubmit={this.handleSubmit}>
            			<div className="row">
            				<h2>Authorization</h2>
            			</div>
            			
            			{this.state.isPasswordWrong ? 
            			<div className="row wrong auth">
            				<h3>Email or password is wrong</h3>
            			</div>
            			: ''}
            			
            			
            			{this.state.isAuth && this.props.user ? 
            			
            			<Redirect to="/account" />
            // 			<div className="row success">
            // 				<h3>Your are {this.props.user.name}. Welcome to the transport information system. </h3>
            // 			</div>
            			: 
            			
            			<div className="row  ">
            			<div className="auth">
        	                <div className="element">
            	                <div className="label">Email</div>
            	                <div className="value">
            	                    <input 
            	                        type="email" 
            	                        placeholder="Email" 
            	                        className="input input-auth input-search" 
            	                        id="authEmail" 
            	                        value={this.state.email}
            	                        onChange={this.handleChangeEmail}
        	                        />
            	                </div>
        	                </div>    
        	                <div className="element">
            	                <div className="label">Password</div>
            	                <div className="value">
            	                    <input 
            	                        type="password" 
            	                        placeholder="Password" 
            	                        className="input input-auth input-search" 
            	                        id="authPass" 
            	                        value={this.state.password}
            	                        onChange={this.handleChangePassword}
        	                        />
            	                </div>
        	                </div>  
        	                <div className="element">
            	                <div className="value">
            	                    <button id="regEnter"  onSubmit={this.handleSubmit}>Log In</button>
            	                </div>
        	                </div> 
        	                <div className="element">
            	                <div className="label registration-text ">
            	                {
            	                   // <NavLink to="/forgot">Forgot your password? </NavLink> 
            	                }
                                    Don't have an account? <NavLink to="/registration">Create one here</NavLink>
                                </div>
        	                </div>   
            			</div>
            			</div>
            			}
        			</form>
                </div>
            </div>
        );
    }
}


const mapStateToProps = store => {
  //console.log(store) 
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUserByEmailAndPassword: (id, password) => dispatch(fetchUserByEmailAndPassword(id, password))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AuthorizationPage))