import React, { Component } from 'react'
import Card from './Card'

import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';

import { fetchStopById } from '../reducers'

class StopCardPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        this.state = {id: props.match.params.id}

        this.props.getStopById(this.state.id)
    }
    render() {

        return (
            <div>
                { this.props.isDownloaded && this.props.stop ? 
                <div>
                    {/*{ console.log('routeCardPageProps', this.props) }*/}
                    {/*{ console.log('routeCardPageState', this.state) }*/}
                    <Card 
                        title="Stop"
                        type="stop"
                        name={this.props.stop.name}
                        points= {[this.props.stop]}
                        routes= {this.props.stop.routes}
                    />
                </div>
                    : ''
                }
            </div>
        );
    }
}

const mapStateToProps = store => {
  //console.log('mapStateToProps', store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        getStopById: id => dispatch(fetchStopById(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(StopCardPage)