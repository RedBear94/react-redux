import React, { Component } from 'react'

import Map from './Map'
import StopsList from './StopsList';
import RoutesList from './RoutesList';

import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
// var BrowserHistory = require('react-router/lib/BrowserHistory').default;

//import { ReactRouter} from 'react-router'
import { browserHistory } from 'react-router'
//var History = ReactRouter.browserHistory;

class Card extends Component {
    constructor(props) {
        super(props);
        this.props = props
        
        //console.log('points', this.props)
        const points = []
        // for (let i = 0; i < this.props.points.length; i++)
        //     points.push(this.props.points[i])

        this.state = { points } 
          
        // this.goBack = this.goBack.bind(this);          
        }
    // static contextTypes = {
    //     router: () => true, // replace with PropTypes.object if you use them
    // }    
  
    // } 
    
    goBack(e) {
        //console.log('history',history)
        //history.go(-1)
        window.history.back();
    //   if (this.props.history) {
    //     e.preventDefault();
    //   } else {
    //     this.props.history.goBack();
    //   }
    }
    
    render() {
        return (
        <section className="container">
        {/* {console.log('Cardprops', this.props)} */}
      
            <div className="card card-route">
                <div className="row">
                    <h2>{this.props.title}</h2>
                </div>
                <div className="row">
                    <div className="div-size-1">
                    
                    {
                        // <button className="back" onClick={browserHistory.goBack}>Back</button>
                        // <button onClick={this.context.router.goBack()}>Back</button>
                        //<button onClick={History.goBack}>Back</button>
                    }
                    {history.length >1 ? <div><button className="go-back-button" onClick={this.goBack}>Back</button></div> : '' }
                        <h3>Name:</h3>
                        <p>{this.props.name}</p>
                        { this.props.type=="stop" ? <RoutesList routes={this.props.routes} /> : 
                        <StopsList points={this.props.points} />
                        }
                    </div>
                    <div className="card-map div-size-2">
                        {
                        // console.log('points', this.props.points ? this.props.points.map(el => [el.point.x, el.point.y]) : '')
                        }
                         {
                         <Map points={this.props.points ? this.props.points.map(el => [el.point.x, el.point.y]) : '' }/>
                         }
                         {
                         //<Map points={this.state.points} />
                         }
                        
                    </div>
            </div>
        </div>   
        </section>
        );
    }
}


export default Card

// const mapStateToProps = store => {
//   console.log('store in mapStateToProps ', store) // посмотрим, что же у нас в store?
//   return store
// }



//export default withRouter(connect(mapStateToProps)(Card))