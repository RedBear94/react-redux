import React, { Component } from 'react'

//import { BrowserRouter, NavLink, Switch, Routes} from 'react-router-dom'
import { BrowserRouter as Router, Route, NavLink, Switch, Routes } from 'react-router-dom'

import Navigation from './Navigation'
import { } from 'react-router-dom'
import MainPage from './MainPage'
import TransportListPage from './TransportListPage'
import TransportCardPage from './TransportCardPage'
import RouteListPage from './RouteListPage'
import RouteCardPage from './RouteCardPage'
import StopListPage from './StopListPage'
import StopCardPage from './StopCardPage'
import AuthorizationPage from './AuthorizationPage'
import RegistrationPage from './RegistrationPage'
import Logout from './Logout'
import Account from './Account'


class Routing extends Component {
    
    constructor (props) {
        super(props);
        this.state = {
            isActive: props.isActive,
            href: props.href,
            title: props.title
        }
    }
    
    render() {
        return (
    		<main>
                    <Switch>
                        <Route exact path='/' component={MainPage}/>
                        { /*<Route exact path='/transports/page/:id' component={TransportListPage}/>*/}
                        <Route exact path='/transports/:id' component={TransportCardPage}/>
                        <Route exact path='/transports' component={TransportListPage}/>
                        <Route exact path='/routes/:id' component={RouteCardPage}/> 
                        <Route exact path='/routes' component={RouteListPage}/>
                        <Route exact path='/stops/:id' component={StopCardPage}/> 
                        <Route exact path='/stops' component={StopListPage}/>
                        <Route exact path='/authorization' component={AuthorizationPage}/>
                        <Route exact path='/registration' component={RegistrationPage}/>
                        <Route exact path='/logout' component={Logout}/>
                        <Route exact path='/account' component={Account}/>
                    </Switch>
            </main>
        );
    }    
}
export default Routing

