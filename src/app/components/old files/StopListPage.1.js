import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { routeHaveStops,  getStopsBlank, getStopsFilter, getStopPage, addRowStop} from '../reducers'

class StoptListPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        //console.log('TTT', this.props.routeHaveStops)
        //this.props.getrouteHaveStops()
        //!this.props.routeHaveStops.length ? this.props.getrouteHaveStops() : ''
        !this.props.getrouteHaveStops.length ? this.props.getrouteHaveStops() : ''
        !this.props.stops.length ? this.props.getStopsBlank() : ''
        //this.props.getStopsBlank()
        
        this.updateDataStop = this.updateDataStop.bind(this);
        this.state = {
            stopName: '',
            stopRoute: '',
            sort: '',
            page: 0,
            isAdding: false,
            }
        this.props.getStopPage(this.state.page)
        
        this.changeisAdding = this.changeisAdding.bind(this);
        this.handleChangeNewName = this.handleChangeNewName.bind(this)
        this.handleChangeNewX = this.handleChangeNewX.bind(this)
        this.handleChangeNewY = this.handleChangeNewY.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    
     updateDataStop  (value) { // {id: 'stopName', value: 'jhjhvjhjh'}
            console.log("StopList", value)
            const t = this.state; // old
            t[value.id] = value.value;
            this.setState({[value.id]: value.value}) 
            //console.log("t", t)
            this.props.getStopsFilter(t.stopName, t.stopRoute, t.sort)
            this.props.getStopPage(0)
            
            // this.setState((prevState) => {
            //     return {value: prevState.value}});
            // this.props.getStopsFilter(value.name, value.route, value.sort)
        }
        
     pageChange (pageNumber) { 
         return () => {
            console.log("pageNumber", pageNumber)
            this.setState({page: pageNumber})
            this.props.getStopPage(pageNumber)
        }
    }
    
    changeisAdding(){
        this.setState(prevState => ({
          isAdding: !prevState.isAdding
        }))
    }
    handleChangeNewName(event) {
        this.setState({newName: event.target.value});
    }
    handleChangeNewX(event) {
        this.setState({newX: event.target.value});
    }
    handleChangeNewY(event) {
        this.setState({newY: event.target.value});
    }
    handleSubmit (event) {
        event.preventDefault();
        if (this.state.newName && this.state.newX && this.state.newY ){
            console.log('form is submitted');
            // const user = this.props.json.users.find(user => user.email == this.state.email )
            // console.log('user', user)
            // if (user)
            //     this.setState({isReg: false, isPasswordWrong: false, isEmailHas: true, isFieldsHave: true});
            // else if (this.state.password !==this.state.password2)
            //     this.setState({isReg: false, isPasswordsWrong: true, isEmailHas: false, isFieldsHave: true});
            // else {
                this.setState({isAdding: true, isSuccess: true, isWrong: false,  isFieldsHave: true});
                this.props.addRowStop(this.state.newName, this.state.newX, this.state.newY);
                this.props.getrouteHaveStops();
                this.props.getStopsBlank();
                this.props.getStopPage(this.props.activePage)
            // }
        } else {
                this.setState({isAdding: true, isWrong: true, isSuccess: false, isFieldsHave: false});
        }
            
    }
    
    render() {
        return (
            <div>
                <Search 
                nameSearch="Search for stops"
                    fields={[
                        {label:'Stop', type:"search", placeholder:"", className:"input input-search", id:"stopName"}, //stopName
        			    {label:'Routes', type:"search", placeholder:"", className:"input input-search", id:"stopRoute"}, //stopRoute
                    ]}
                    updateData={this.updateDataStop}
                />
                
    {this.props.user && this.props.user.isAdmin ? 
    <div className="container"> 
        <div className="card">
             <div className="row">
                <button onClick={this.changeisAdding}>Add a new stop</button>
             </div>
             
             
              {this.state.isAdding ? 
                 <div className="row">
                 <div className="auth">
                    <form onSubmit={this.handleSubmit}>
                    <div className="element">
    	                <div className="label">Name</div>
    	                <div className="value">
    	                    <input 
    	                        type="text" 
    	                        placeholder="Name" 
    	                        className="input input-auth input-search" 
    	                        id="newStopName" 
    	                        value={this.state.newName}
    	                        onChange={this.handleChangeNewName}
	                        />
    	                </div>
	                </div>  
	                
                    <div className="element">
    	                <div className="label">X</div>
    	                <div className="value">
    	                    <input 
    	                        type="text" 
    	                        placeholder="X" 
    	                        className="input input-auth input-search" 
    	                        id="newStopX" 
    	                        value={this.state.newX}
    	                        onChange={this.handleChangeNewX}
	                        />
    	                </div>
	                </div> 
                    <div className="element">
    	                <div className="label">Y</div>
    	                <div className="value">
    	                    <input 
    	                        type="text" 
    	                        placeholder="Y" 
    	                        className="input input-auth input-search" 
    	                        id="newStopY" 
    	                        value={this.state.newY}
    	                        onChange={this.handleChangeNewY}
	                        />
    	                </div>
	                </div>  
	                <div className="element">
    	                <div className="value">
    	                    <button id="stopAddEnter"  onSubmit={this.handleSubmit}>Add</button>
    	                </div>
	                </div> 
	                </form>
                </div>
             </div>
             : ''}
        </div>
    </div>
    : ''}
                
    <div className="container">
        <div className="card card-list">
            
            {
            this.props.stopsPage && this.props.stopsPage.length ?
                this.props.stopsPage.map(stop => {
                    return <div className="row">
                                <div className="element">
                            	    <p className="label">Stop name</p>
                            		<p className="value">
                            		    <NavLink to={'/stops/'+stop.id}> {stop.name} </NavLink>
                        		    </p>
                                </div>
                                <div className="element">
                            	    <p className="label">Route</p>
                            		<p className="value list">
                            	    {
                            	        stop.routes ? stop.routes.map(route => {
                            	           return (
                            	            <NavLink to={'/routes/'+route.id}> {route.name}  </NavLink>
                            	        )}) : ''
                            		}
                            	    </p>
                                </div>
                            </div>
                        })
                    : <div className="no-elements"><p>No results found</p></div>
                }
        </div>
    </div>
        <div className="container pagination">
        {console.log('totalPages', this.props.totalPages)}
        { [...Array(this.props.totalPages).keys()].map(i=> {
            return <button onClick={this.pageChange(i)} className={ i === this.props.activePage ?'active': ''}> {i+1} </button>
        })
        }
        </div>
</div>           
        );
    }
}

const mapStateToProps = store => {
  console.log(store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        //getrouteHaveStops: StopRoutes => dispatch(routeHaveStops()),
        getrouteHaveStops: () => dispatch(routeHaveStops()),
        getStopsBlank: () => dispatch(getStopsBlank()),
        getStopsFilter: (name, route, sort) => dispatch(getStopsFilter(name, route, sort)),
        getStopPage: pageNumber => dispatch(getStopPage(pageNumber)),
        addRowStop:  (newName, newX, newY) => dispatch(addRowStop(newName, newX, newY)),
    }
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(StoptListPage))
