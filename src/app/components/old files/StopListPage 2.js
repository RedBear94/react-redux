import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { routeHaveStops,  getStopsBlank, getStopsFilter, getStopPage} from '../reducers'

class StoptListPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        //console.log('TTT', this.props.routeHaveStops)
        //this.props.getrouteHaveStops()
        //!this.props.routeHaveStops.length ? this.props.getrouteHaveStops() : ''
        !this.props.getrouteHaveStops.length ? this.props.getrouteHaveStops() : ''
        !this.props.stops.length ? this.props.getStopsBlank() : ''
        //this.props.getStopsBlank()
        
        this.updateDataStop = this.updateDataStop.bind(this);
        this.state = {
            stopName: '',
            stopRoute: '',
            sort: '',
            page: 0,
            }
        this.props.getStopPage(this.state.page)
    }

    
     updateDataStop  (value) { // {id: 'stopName', value: 'jhjhvjhjh'}
            console.log("StopList", value)
            const t = this.state; // old
            t[value.id] = value.value;
            this.setState({[value.id]: value.value}) 
            //console.log("t", t)
            this.props.getStopsFilter(t.stopName, t.stopRoute, t.sort)
            this.props.getStopPage(0)
            
            // this.setState((prevState) => {
            //     return {value: prevState.value}});
            // this.props.getStopsFilter(value.name, value.route, value.sort)
        }
        
     pageChange (pageNumber) { 
         return () => {
            console.log("pageNumber", pageNumber)
            this.setState({page: pageNumber})
            this.props.getStopPage(pageNumber)
        }
    }

    
    render() {
        return (
            <div>
                <Search 
                nameSearch="Search for stops"
                    fields={[
                        {label:'Stop', type:"search", placeholder:"", className:"input input-search", id:"stopName"}, //stopName
        			    {label:'Routes', type:"search", placeholder:"", className:"input input-search", id:"stopRoute"}, //stopRoute
                    ]}
                    updateData={this.updateDataStop}
                />
                
                
    <div className="container">
        <div className="card card-list">
            
            {
            this.props.stopsPage && this.props.stopsPage.length ?
                this.props.stopsPage.map(stop => {
                    return <div className="row">
                                <div className="element">
                            	    <p className="label">Stop name</p>
                            		<p className="value">
                            		    <NavLink to={'/stops/'+stop.id}> {stop.name} </NavLink>
                        		    </p>
                                </div>
                                <div className="element">
                            	    <p className="label">Route</p>
                            		<p className="value list">
                            	    {
                            	        stop.routes ? stop.routes.map(route => {
                            	           return (
                            	            <NavLink to={'/routes/'+route.id}> {route.name}  </NavLink>
                            	        )}) : ''
                            		}
                            	    </p>
                                </div>
                            </div>
                        })
                    : <div className="no-elements"><p>No results found</p></div>
                }
        </div>
    </div>
        <div className="container pagination">
        {console.log('totalPages', this.props.totalPages)}
        { [...Array(this.props.totalPages).keys()].map(i=> {
            return <button onClick={this.pageChange(i)} className={ i === this.props.activePage ?'active': ''}> {i+1} </button>
        })
        }
        </div>
</div>           
        );
    }
}

const mapStateToProps = store => {
  console.log(store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        //getrouteHaveStops: StopRoutes => dispatch(routeHaveStops()),
        getrouteHaveStops: () => dispatch(routeHaveStops()),
        getStopsBlank: () => dispatch(getStopsBlank()),
        getStopsFilter: (name, route, sort) => dispatch(getStopsFilter(name, route, sort)),
        getStopPage: pageNumber => dispatch(getStopPage(pageNumber)),
    }
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(StoptListPage))
