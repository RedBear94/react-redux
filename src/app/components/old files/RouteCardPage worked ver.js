import React, { Component } from 'react'
import Card from './Card'

import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';

import { fetchRouteById } from '../reducers'

//store.dispatch(fetchRouteById())

class RouteCardPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        this.state = {id: props.match.params.id}
        
        this.props.getRouteById(this.state.id)
    }
    render() {

        return (
            <div>
                { this.props.isDownloaded  ? 
                <div>

                    { console.log('routeCardPageProps', this.props) }
                    { console.log('routeCardPageState', this.state) }
  
                    <Card 
                    title="Route"
                    name={this.props.route.name}
                    //name={this.props.route.returnStops[0].name}
                    points= {this.props.route.returnStops}
                   
                    //     [
                    //         //this.props.route.returnStops[0]
                    //         { name: 'Проспект Героев', id: 4, point: {x: 59.859217, y: 30.155995}},
                    //         // { name: 'Улица Доблести', point: [59.859962, 30.178426]},
                    //         // { name: 'Проспект Кузнецова', point: [59.856863, 30.186717]},
                    //         // { name: 'Улица Десантников', point: [59.854097, 30.201090]},
                    //         // { name: 'Проспект Маршала Жукова', point: [59.849790, 30.218527]},
                    //     ]
                    // }
                    />
                    
                    </div>
                    : <p> Данные загружаются...</p>
                }
            </div>
        );
    }
}


const mapStateToProps = store => {
  console.log('mapStateToProps', store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        getRouteById: id => dispatch(fetchRouteById(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RouteCardPage)