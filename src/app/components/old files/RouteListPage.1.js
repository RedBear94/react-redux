import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { CrossRoutes } from '../reducers'

class RouteListPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        this.props.getCrossRoutes()
    }
    
    render() {
        console.log("PROPS", this.props)
        
        return (
            <div>
                <Search 
                nameSearch="Search for routes"
                    fields={[
                        {label:'Route', type:"search", placeholder:"", className:"input input-search", id:"Route"},
        			    {label:'Сross paths', type:"search", placeholder:"", className:"input input-search", id:"Cross routes"},
                    ]}
                />
                
        <div className="container">
            <div className="card card-list">
                {
                this.props.crossRoutes ?
                    Object.values(this.props.crossRoutes).map(el => {
                        return <div className="row">
                                    <div className="element">
                                	    <p className="label">Route</p>
                                		<p className="value">
                                		    <NavLink to={'/routes/'+el.id}> {el.name} </NavLink>
                            		    </p>
                                    </div>
                                    <div className="element">
                                	    <p className="label">Cross Path</p>
                                		<p className="value">
                                	    {
                                	        el.crossRoutesObj ? el.crossRoutesObj.map(obj => {
                                	        //console.log(el.routes)
                                	           return (
                                	            <NavLink to={'/routes/'+obj.id}> {obj.route}  </NavLink>
                                	        )}) : ''
                                		}
                                	    </p>
                                    </div>
                                </div>
                            })
                        : ''
                    
                    /*this.props.json.routes ?
                    this.props.json.routes.map((el, i) => {
                    return <div className="row">
                            	<div className="element">
                            	    <p className="label">Route</p>
                            		<p className="value"><NavLink to={'/routes/'+el.id}>
                            	    {
                            		    el.name //? this.props.json.routes.find(route => route.id == el.route).name : '-'
                            		}
                            	    </NavLink></p>
                                </div>
                                
                                <div className="element">
                            	    <p className="label">Сross paths</p>
                            	    <p className="value">
                                    {
                                        //console.log("Elem", el)
                                    }
                            	    </p>
                                </div>
                                */
                                /*
                                <div className="element">
                            	    <p className="label">Сross paths</p>
                            	    <p className="value">
                                    {
                                        console.log("Elem", el)
                                    }
                                    {    this.props.json.routes.filter(routeJS => {
                                            console.log('rroute', routeJS)
                                            return (el.id!==routeJS.id && 
                                                routeJS.stops.some(stopJS=> {
                                                    console.log('sstop', stopJS, el.stops, el.stops.indexOf(stopJS) !== -1)
                                                    return (el.stops.indexOf(stopJS) !== -1)
                                                })
                                            )
                                        })
                                        .map((route, i)=>
                                        <span>
                                            {i===0? '' : ', '}
                                            <span>
                                                {console.log("stop", route)}
                                                <NavLink to={'/routes/'+route.id}>{route.name}</NavLink>
                                            </span>
                                        </span>
                                                
                                        )
                                    }
                            	    </p>
                                </div>*/
                            /*</div>
                        })
                    : ''*/
                }
                
            </div>
        </div>
    </div>
        );
    }
}


const mapStateToProps = store => {
  console.log(store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCrossRoutes: stopsHaveRoute => dispatch(CrossRoutes())
    }
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(RouteListPage))
//export default RouteListPage