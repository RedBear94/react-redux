import React, { Component } from 'react'
import Search from "./Search"
import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';
import { routeHaveStops } from '../reducers'

class StoptListPage extends Component {
    constructor (props) {
        super(props);
        this.props = props
        this.props.getrouteHaveStops()
    }
    
    render() {
        return (
            <div>
                <Search 
                nameSearch="Search for stops"
                    fields={[
                        {label:'Stop', type:"search", placeholder:"", className:"input input-search", id:"Stop"},
        			    {label:'Routes', type:"search", placeholder:"", className:"input input-search", id:"Routes"},
                    ]}
                />
                
    <div className="container">
        <div className="card card-list">
            {
            this.props.json.stops ?
                    this.props.json.stops.map((el, i) => {
                    return <div className="row">
                                <div className="element">
                            	    <p className="label">Name Stop</p>
                            		<p className="value"><NavLink to={'/stops/'+el.id}>
                            	    {
                            		    el.name //? this.props.json.routes.find(route => route.id == el.route).name : '-'
                            		}
                            	    </NavLink></p>
                                </div>
                                <div className="element">
                            	    <p className="label">Route</p>
                            		<p className="value"><NavLink to={'/routes/'+el.id}>
                            	    {
                            	        //(this.props.routeHaveStops.length!==0) ? console.log("1234", this.props.routeHaveStops) : ''
                            	        //(this.props.routeHaveStops.length!==0) ? (this.props.routeHaveStops[el.id].stops.map(ell=>{return (ell+' ')})) : ''
                            	        (this.props.routeHaveStops.length!==0) ? (this.props.routeHaveStops[el.id].stops.map(ell=>{return (ell+' ')})) : ''
                            		}
                            	    </NavLink></p>
                                </div>
                            </div>
                        })
                    : ''
                }
        </div>
    </div>
</div>           
        );
    }
}

const mapStateToProps = store => {
  console.log(store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        getrouteHaveStops: StopRoutes => dispatch(routeHaveStops())
    }
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(StoptListPage))
