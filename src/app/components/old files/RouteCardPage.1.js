import React, { Component } from 'react'
import Card from './Card'

import { connect } from 'react-redux'
import { withRouter, NavLink } from 'react-router-dom';

import { fetchRouteById } from '../reducers'




class RouteCardPage extends Component {
    constructor ({ match }) {
        super();
        this.state = {id: match.params.id, route : {}}
        //this.getRouteById = this.getRouteById.bind(this);
    }
    
    // getRouteById(id, routes) {
    //     if (routes) {
    //         const route = routes.find(route => route.id == id)
    //         console.log('route', route)
    //         //this.setState({ route: route})
    //         this.state.route = route
    //     }
    // }
    
    // shouldComponentUpdate(nextProps, nextState) {
    //             this.getRouteById(this.state.id, nextProps.json.routes )
        
    //     //this.props = nextProps
    //     //this.state = nextState
    //     this.render()
    // }
    
    render() {
        const {getRouteById} = this.props;
        return (
            <div>
                {/*console.log('isName',this.state.route.name ? this.state.route.name : '-')*/}
                { this.props.isDownloaded  ? 
                <div>
                    { this.props.getRouteById(this.state.id)}
                    { console.log('RouteCardPageProps', this.props)}
                    {/*this.state.id.toString()*/}
                    { console.log('routesroutes1', this.props.routes[1]) }
                    { console.log('routesroutes2', this.props.routes["1"]) }
                    { console.log('routesroutes2', this.props.routes) }
                    <Card 
                    title="Route"
                    name={this.props.routes[this.state.id].name }
                    
                    points= {
                    // this.state.id && this.props.json.routes ? 
                    //     this.props.json.routes.find(route => route.id == this.state.id) 
                        
                    //     :'-'
                    [
                        { name: 'Проспект Героев', point: [59.859217, 30.155995]},
                        { name: 'Улица Доблести', point: [59.859962, 30.178426]},
                        { name: 'Проспект Кузнецова', point: [59.856863, 30.186717]},
                        { name: 'Улица Десантников', point: [59.854097, 30.201090]},
                        { name: 'Проспект Маршала Жукова', point: [59.849790, 30.218527]},
                        ]
                        }
                   
                    />
                    </div>
                    : <p>Данные загружаются...</p>
                }
            </div>
        );
    }
}


const mapStateToProps = store => {
  console.log('mapStateToProps', store) // посмотрим, что же у нас в store?
  return store
}

const mapDispatchToProps = (dispatch) => {
    return {
        getRouteById: id => dispatch(fetchRouteById(id))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(RouteCardPage)