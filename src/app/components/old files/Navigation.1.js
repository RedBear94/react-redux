import React, { Component } from 'react'

class NavLogo extends Component {
    constructor (props) {
        super(props);
    }
    
    render() {
        return (
            <div className="logo">
				<a href="/">
					<img src="/static-assets/img/car-logo2.svg" alt="" />
				</a>
			</div>
        );
    }
}


class NavElement extends Component {
    
    constructor (props) {
        super(props);
        this.state = {
            isActive: props.isActive,
            href: props.href,
            title: props.title
        }
    }
    
    render() {
        return (
    		<li className={this.state.isActive ? 'active' : ''}><a href={this.state.href}>{this.state.title}</a></li>
        );
    }    
}

class NavElements extends Component {
    constructor (props) {
        super(props);
        this.props = props,
        this.state = {
            isVisable: props.isVisable,
            links: [
                 { isActive: true, href: '/', title: 'Home'}, 
                 { isActive: false, href: '#', title: 'The Stops'}, 
                 { isActive: false, href: 'routeCard', title: 'Routes'}, 
                 { isActive: false, href: 'transportsTable', title: 'Car Park'}, 
                 { isActive: false, href: '#', title: 'Sign in'}, 
                ]
        }
    }
    
    render() {
        return (
            	<nav className={this.props.isVisable ? 'visable': 'unvisable'} id="nav">
    				<ul>
    				{
                        this.state.links.map( el => {
                        return <NavElement isActive={el.isActive} href={el.href} title={el.title} /> 
                        })
    				}
    				   
    				</ul>
    			</nav>
        );
    }
}

class NavBurger extends Component {
    constructor (props) {
        super(props);
        this.state = {
            isOpen : true,
            updateDataIsOpen : props.updateDataIsOpen
        };
        this.burgerSwitch = this.burgerSwitch.bind(this);
    }
    
    burgerSwitch() {
        this.setState({isOpen: !this.state.isOpen});
        this.state.updateDataIsOpen(this.state.isOpen);
    }
    
    
    render() {
        const isOpen = this.state.isOpen;
        return (
            <nav>
				<ul>
					<li className="burger"><a href="javascript:void(0);" className="icon" onClick={this.burgerSwitch}><i className="fa fa-bars"></i></a></li>
				</ul>
			</nav>
        );
    }
}




class Navigation extends Component {
    constructor (props) {
        super(props);
        this.state = { isOpen: false }
        this.updateDataIsOpen = this.updateDataIsOpen.bind(this);
    }
    
    
    updateDataIsOpen (value) {
        //console.log(value)
        this.setState({ isOpen: value });
        console.log("Update: "+ this.state.isOpen);
    }
  
    
    render() {
        return (
        <div>
            <header className="header">
        		<div className="container">
        		    <NavLogo />
        			<NavBurger updateDataIsOpen={this.updateDataIsOpen} />
        			<NavElements isVisable={this.state.isOpen} />
        		</div>
            </header>	
            {/*
            <div className="welcome">
            	<div className="container">
            		<div className="row">
            			<p className="hello-text">Welcome to the transport information system</p>
            		</div>
            	</div>
            </div>
            */}
                
            </div>
        );
    }
}
export default Navigation