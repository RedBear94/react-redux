import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import { render } from 'react-dom'
import { browserHistory } from 'react-router'

import { Provider } from 'react-redux'
import { store } from './store/configureStore'

import { fetchAllItemsFromServer } from './reducers'


// import { ConnectedRouter } from 'react-router-redux'

// store.subscribe(() =>
//          console.log(store.getState())
//         )
store.dispatch(fetchAllItemsFromServer())
//const state = store.getState()

//var History = ReactRouter.browserHistory;

//var BrowserHistory = require('react-router/lib/BrowserHistory').default;

ReactDOM.render(
     <Provider store={store}>
        {/*<BrowserRouter>*/}
        <Router>
            <App />
        </Router>
        {/*</BrowserRouter>*/}
    </Provider>
    , document.getElementById('root'));
