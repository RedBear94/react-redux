import { createStore, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import { cardReducer } from '../reducers'
import thunk from 'redux-thunk';

//export const store = createStore(cardReducer, initialState)

export default function configureStore() {
    let store = createStore(cardReducer, applyMiddleware(thunk))
    return store
}

export const store = configureStore()

//export const store = createStore(cardReducer)

//store.dispatch(fetchAllItemsFromServer())